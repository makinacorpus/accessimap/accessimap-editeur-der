'use strict';

describe('Service: InteractionService', function () {
    beforeEach(module('accessimapEditeurDerApp'));

    var InteractionService;

    beforeEach(inject(function ($injector) {
        InteractionService = $injector.get('InteractionService')

        // Prepare test data: Filters.
        InteractionService.addFilter('Filter 1', 'f1');
        InteractionService.addFilter('Filter 2', 'f2');
        InteractionService.addFilter('Filter 3', 'f3');
        // Prepare test data: Interactions.
        InteractionService.setInteraction('poi-1', 'Mon POI 1', 'f1', 'Valeur 1',  'tap',        'tts', 'iuuid1');
        InteractionService.setInteraction('poi-1', null,        'f1', 'Valeur 2',  'double_tap', 'tts', 'iuuid2');
        InteractionService.setInteraction('poi-2', null,        'f1', 'Valeur 3',  'tap',        'tts', 'iuuid3');
        InteractionService.setInteraction('poi-2', null,        'f1', 'Valeur 4',  'double_tap', 'tts', 'iuuid4');
        InteractionService.setInteraction('poi-3', null,        'f1', 'Valeur 5',  'tap',        'tts', 'iuuid5');
        InteractionService.setInteraction('poi-3', null,        'f2', 'Valeur 6a', 'tap',        'tts', 'iuuid6a');
        InteractionService.setInteraction('poi-3', null,        'f2', 'Valeur 6b', 'tap',        'tts', 'iuuid6b');
        InteractionService.setInteraction('poi-3', null,        'f2', 'Valeur 6c', 'double_tap', 'tts', 'iuuid6c');
        InteractionService.setInteraction('poi-3', null,        'f3', 'Valeur 7',  'double_tap', 'tts', 'iuuid7');

        InteractionService.setInteraction('poi-4', null, 'f1', 'Valeur 8',  'tap',        'tts', 'iuuid8');
        InteractionService.setInteraction('poi-4', null, 'f2', 'Valeur 9',  'double_tap', 'tts', 'iuuid9');
        InteractionService.setInteraction('poi-4', null, 'f3', 'Valeur 10', 'tap',        'tts', 'iuuid10');
    }));

    describe('The InteractionService', function() {
        it('should be defined', function () {
            expect(InteractionService).toBeDefined();
        });

        it('should correctly handle creation of filters', function () {
            var filters = InteractionService.getFilters();

            expect(filters.length).toBe(3);

            expect(filters[0].id).toBe('f1');
            expect(filters[0].name).toBe('Filter 1');

            expect(filters[1].id).toBe('f2');
            expect(filters[1].name).toBe('Filter 2');

            expect(filters[2].id).toBe('f3');
            expect(filters[2].name).toBe('Filter 3');
        });

        it('should correctly handle creation of interactions', function () {
            var interactions = InteractionService.getInteractions();

            expect(Object.keys(interactions).length).toBe(4);

            expect(interactions['poi-1'].label).toBe('Mon POI 1');
            expect(interactions['poi-2'].label).toBe('');

            var firstInteraction = InteractionService.getInteractionByUUID('iuuid1');

            expect(firstInteraction.gesture).toBe('tap');
            expect(firstInteraction.value).toBe('Valeur 1');
            expect(firstInteraction.protocol).toBe('tts');
            expect(firstInteraction.filter).toBe('f1');
            expect(firstInteraction.uuid).toBe('iuuid1');

            expect(interactions['poi-1'].interactions.length).toBe(2);
            expect(interactions['poi-2'].interactions.length).toBe(2);
            expect(interactions['poi-3'].interactions.length).toBe(5);
        });

        it('should flag invalid filters', function () {
            var interactions = InteractionService.getInteractions();

            expect(interactions['poi-1'].invalidFilterIds).toEqual([]);
            expect(interactions['poi-2'].invalidFilterIds).toEqual([]);
            expect(interactions['poi-3'].invalidFilterIds).toEqual(['f2']);
        });

        it('should correctly handle deletion of interactions', function () {
            var interactions = InteractionService.getInteractions();

            expect(interactions['poi-3'].interactions.length).toEqual(5);
            expect(interactions['poi-3'].interactions[2].uuid).toEqual('iuuid6b');
            expect(interactions['poi-3'].invalidFilterIds).toEqual(['f2']);

            InteractionService.removeInteractionByUUID('iuuid6b');

            expect(interactions['poi-3'].interactions.length).toEqual(4);
            expect(interactions['poi-3'].interactions[2].uuid).toEqual('iuuid6c');
            expect(interactions['poi-3'].invalidFilterIds).toEqual([]);
        });

        it('should correctly handle duplication of interactions', function () {
            var interactions = InteractionService.getInteractions();

            expect(interactions['poi-4'].interactions.length).toEqual(3);

            var firstInteraction = InteractionService.getInteractionByUUID('iuuid8');

            InteractionService.duplicateInteractionToAllFilters('poi-4', firstInteraction);

            expect(interactions['poi-4'].interactions.length).toEqual(4);

            expect(InteractionService.getInteractionByUUID('iuuid8')).toBeNull();
            expect(InteractionService.getInteractionByUUID('iuuid9')).toBeDefined();
            expect(InteractionService.getInteractionByUUID('iuuid10')).toBeNull();

            expect(interactions['poi-4'].interactions[0].value).toEqual('Valeur 9');
            expect(interactions['poi-4'].interactions[1].value).toEqual('Valeur 8');
            expect(interactions['poi-4'].interactions[2].value).toEqual('Valeur 8');
            expect(interactions['poi-4'].interactions[3].value).toEqual('Valeur 8');
            expect(interactions['poi-4'].interactions[0].filter).toEqual('f2');
            expect(interactions['poi-4'].interactions[1].filter).toEqual('f1');
            expect(interactions['poi-4'].interactions[2].filter).toEqual('f2');
            expect(interactions['poi-4'].interactions[3].filter).toEqual('f3');
            expect(interactions['poi-4'].interactions[0].gesture).toEqual('double_tap');
            expect(interactions['poi-4'].interactions[1].gesture).toEqual('tap');
            expect(interactions['poi-4'].interactions[2].gesture).toEqual('tap');
            expect(interactions['poi-4'].interactions[3].gesture).toEqual('tap');
        });
    });
});
