/**
 * @ngdoc service
 * @name accessimapEditeurDerApp.ToolboxImageService
 * @description
 * Expose different methods to draw on the d3 svg area
 */
(function() {
    'use strict';

    function ToolboxImageService() {

        this.init               = init;

        var svgDrawing,
            applyStyle ;

        function init(_svgDrawing, _applyStyle) {
            svgDrawing = _svgDrawing;
            applyStyle = _applyStyle;
        }

        /**
         * @ngdoc method
         * @name  importImage
         * @methodOf accessimapEditeurDerApp.ToolboxImageService
         *
         * @description
         *
         * Import a local image in the DER
         *
         */
        function importImage(x, y, style, color, contour) {

        }
    }

    angular.module(moduleApp).service('ToolboxImageService', ToolboxImageService);

    ToolboxImageService.$inject = [];

})();
