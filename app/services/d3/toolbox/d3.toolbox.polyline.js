/**
 * @ngdoc service
 * @name accessimapEditeurDerApp.ToolboxPolylineService
 * @description
 * Expose different methods to draw on the d3 svg area
 */
(function() {
    'use strict';

    function ToolboxPolylineService(RadialMenuService, GeneratorService, UtilService, HistoryService) {

        this.init                  = init;
        this.beginLineOrPolygon    = beginLineOrPolygon;
        this.drawHelpLineOrPolygon = drawHelpLineOrPolygon;
        this.finishLineOrPolygon   = finishLineOrPolygon;

        var svgDrawing,
            applyStyle;

        function init(_svgDrawing, _applyStyle) {
            svgDrawing = _svgDrawing;
            applyStyle = _applyStyle;
        }

        /**
         * Starts a loop checking regularly if an object is being edited. When no
         * more edition is going on, stops the loop and saves the state.
         */
        function saveStateOnceEditionIsDone() {
            setTimeout(
                function() {
                    // If no more edition is going on, save the state.
                    if (!d3.select('.edition')[0][0]) {
                        HistoryService.saveState();
                        HistoryService.enableUndoRedo();
                    }
                    // User is still editing, wait longer.
                    else {
                        saveStateOnceEditionIsDone();
                    }
                },
                100
            );
        }

        function beginLineOrPolygon(x, y, style, color, contour, mode, lastPoint, lineEdit) {
            var drawingLayer = svgDrawing.select('g[data-name="' + mode + 's-layer"]'),
                path,
                pathInner;

            // User placed a line.
            // Carry on with the next line / polygon.
            if (d3.select('.edition')[0][0]) {
                path = d3.select('.edition');

                if (mode === 'line') {
                    pathInner = d3.select('.edition.inner');
                }

            // User starts creating a polygon.
            } else {
                var iid = UtilService.getNextUnusedIid();
                path = drawingLayer
                        .append('path')
                        .attr('data-link', iid)
                        .classed('link_' + iid, true)
                        .attr({'class': 'edition'});


                applyStyle(path, style.style, color, false);

                if (contour && !path.attr('stroke')) {
                    path.attr('stroke', 'black')
                        .attr('stroke-width', '2');
                }

                if (mode === 'line') {
                    pathInner = drawingLayer
                                .append('path')
                                .attr({'class': 'edition inner'});
                    applyStyle(pathInner, style.styleInner, color, false);
                }

                // Starts a loop waiting for the polygon to be completed before saving the sate.
                saveStateOnceEditionIsDone();
                // Prevents user from using history features while creating a polygon.
                HistoryService.disableUndoRedo();
            }

            lineEdit.push([x, y]);
            path.attr({
                d: GeneratorService.pathFunction[mode](lineEdit)
            });

            if (mode === 'line') {
                pathInner.attr({
                    d: GeneratorService.pathFunction[mode](lineEdit)
                });
            }
            applyStyle(path, style.style, color, false);

            if (contour && !path.attr('stroke')) {
                path.attr('stroke', 'black')
                    .attr('stroke-width', '2');
            }

            if (mode === 'line') {
                applyStyle(pathInner, style.styleInner, color, false);
            }
        }

        /**
         * Updates the line being placed by user when they are drawing a line of a polygon.
         *
         * @param {Number} x
         *   Target X position.
         * @param {Number} y
         *   Target Y position.
         * @param {Object} style
         *   The 'style' property as returned by EditController::getDrawingParameters().
         * @param {Object} color
         *   The 'color' property as returned by EditController::getDrawingParameters().
         * @param {Boolean} contour
         *   The 'contour' property as returned by EditController::getDrawingParameters().
         * @param {String} mode
         *   The 'mode' property as returned by EditController::getDrawingParameters().
         * @param {Object|null} startPoint
         *   Starting position of the line.
         *   Set to null if the user is currently positionning the starting point.
         * @param {Boolean} shiftKeyPressed
         *   Whether the shift key is being pressed. When so, line being placed will stick
         *   to horizontal and vertical lines crossing "lastPoint".
         *
         * @returns {Object}
         *   Current destination point that will be pointer's current position most of the time,
         *   but may be different when the shift key is being pressed.
         *   Structure: {'x': Number, 'y': Number}.
         */
        function drawHelpLineOrPolygon(x, y, style, color, contour, mode, startPoint, shiftKeyPressed) {
            if (startPoint) {
                var drawingLayer = svgDrawing.select('g[data-name="' + mode + 's-layer"]'),
                    line;

                if (d3.select('.ongoing')[0][0]) {
                    line = d3.select('.ongoing');
                } else {
                    line = drawingLayer
                        .append('line')
                        .attr({'class': 'ongoing'});
                    applyStyle(line, style.style, color, false);

                    if (contour && !line.attr('stroke')) {
                        line.attr('stroke', 'black')
                            .attr('stroke-width', '2');
                    }
                }

                // If the shiftKey is pressed, draw horizontal or vertical lines
                // with a tolerance of 5°.
                if (shiftKeyPressed) {
                    var tanAngle = Math.abs((y - startPoint.y) / (x - startPoint.x));
                    var tan5     = Math.tan((5 * 2 * Math.PI) / 360);
                    var tan85    = Math.tan((85 * 2 * Math.PI) / 360);

                    if (tanAngle < tan5) {
                        y = startPoint.y;
                    }
                    else if (tanAngle > tan85) {
                        x = startPoint.x;
                    }
                }

                line.attr('x1', startPoint.x)
                    .attr('y1', startPoint.y)
                    .attr('x2', x)
                    .attr('y2', y);
            }

            return {'x': x, 'y': y};
        }

        function finishLineOrPolygon(style, color, mode) {
            var path = d3.select('.edition');

            if (mode === 'line') {
                d3.select('.edition.inner')
                    .classed('edition', false);
            }

            path.attr('e-style', style.id)
                .attr('e-color', color.color);

            path.classed('edition', false)
                .attr('data-type', mode)
                .attr('data-from', 'drawing');

            d3.select('.ongoing').remove();
        }
    }

    angular.module(moduleApp).service('ToolboxPolylineService', ToolboxPolylineService);

    ToolboxPolylineService.$inject = ['RadialMenuService', 'GeneratorService', 'UtilService', 'HistoryService'];

})();
