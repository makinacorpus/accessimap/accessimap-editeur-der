(function() {
    'use strict';
    /**
     * @ngdoc overview
     * @name accessimapEditeurDerApp
     * @description
     * Main module of the application.
     */
    var moduleApp = 'accessimapEditeurDerApp';

    window.moduleApp = moduleApp;

    angular
        .module(moduleApp, [
            'ngAnimate',
            'ngCookies',
            'ngResource',
            'ngRoute',
            'ngSanitize',
            'ngTouch',
            'ui.select',
            'ui.bootstrap',
            'ui.bootstrap-slider',
            'ui.bootstrap.tabs',
            'ui.bootstrap.tooltip',
            'bootstrap.fileField'
        ])
        .config(function($routeProvider) {
            $routeProvider
                .when('/', {
                    templateUrl: 'scripts/routes/home/template.html',
                    controller: 'HomeController',
                    controllerAs: '$ctrl'
                })
                .when('/edit', {
                    templateUrl: 'scripts/routes/edit/template.html',
                    controller: 'EditController',
                    controllerAs: '$ctrl'
                })
                .otherwise({
                    redirectTo: '/'
                });
        })
        .config(function(uiSelectConfig) {
            uiSelectConfig.dropdownPosition = 'down';
        })
        // .config(function ($opbeatProvider) {
        //     $opbeatProvider.config({
        //         orgId: '43f4c77b74f64097ab04194c87d98086',
        //         appId: '7bcd582124'
        //     })
        // });

    /**
     * Directive for having a file-input that reacts to all file changes, even if
     * the file is the same than the previous one.
     *
     * Highly inpired from dtruel's work, see their repo for readme and license:
     * https://github.com/dtruel/angular-file-input
     */
    angular.module(moduleApp).directive("fileInput",["$parse",function($parse) {
      return {
        link: function($scope, $element, $attrs, $ngModelCtrl){
          var fileInput = null;

          function createFileInput() {
            fileInput = document.createElement("input");
            fileInput.type = "file";
            fileInput.style = "display: none;";

            // If an 'accept' attribute was provided, add it to the input.
            if ($attrs.accept) {
              fileInput.accept = $attrs.accept;
            }

            angular.element(fileInput).on('click',function(e){
              e.stopPropagation();
              fileInput.click();
            });

            angular.element(fileInput).on("change",function(event){
              $scope.$apply(function(){
                $parse($attrs.onChange)($scope, {$event:event});
              })
              // Remove old input so it does not keep the selected file.
              // This makes it possible to fire the onchange event repeatedly
              // even if user selects the same file over and over.
              fileInput.remove();
              // Recreate the file input.
              createFileInput();
            })

            $element.append(fileInput);
          }

          createFileInput();

          $element.bind('click',function(e){
            e.preventDefault();
            fileInput.click();
          });
        }
      }
    }]);
})();
