/**
 * @ngdoc service
 * @name accessimapEditeurDerApp.UtilService
 * @description
 * Service exposing utils functions
 */
(function() {
    'use strict';

    function UtilService($q) {

        this.convertImgToBase64   = convertImgToBase64;
        this.getNextUnusedIid     = getNextUnusedIid;
        this.getUUID              = getUUID;
        this.getFileInfoFromInput = getFileInfoFromInput;

        /**
         * @ngdoc method
         * @name  convertImgToBase64
         * @methodOf accessimapEditeurDerApp.UtilService
         *
         * @description
         * Convert a png tile into a base64 image
         *
         * Useful for later edition in software like Inkscape for better display / edition
         * @param  {Object} tile [description]
         */
        function convertImgToBase64(tile) {
            if (tile) {
                var img = new Image();
                img.crossOrigin = 'Anonymous';
                img.src = d3.select(tile).attr('href');
                img.onload = function() {
                    var canvas = document.createElement('CANVAS'),
                        ctx = canvas.getContext('2d');
                    canvas.height = this.height;
                    canvas.width = this.width;
                    ctx.drawImage(this, 0, 0);
                    var dataURL = canvas.toDataURL('image/png');
                    d3.select(tile).attr('href', dataURL);
                };
            }
        }

        /**
         * ngdoc method
         * @name  getNextUnusedIid
         * @methodOf accessimapEditeurDerApp.UtilService
         *
         * @description
         * Function returning a unique identifier not yet used in the drawing.
         *
         * @return {integer} Fresh id !
         */
        function getNextUnusedIid() {
            var unusedIid = 1;

            // Gather all objects having an iid.
            /** @var Selection allIidObjects */
            var allIidSelection = d3.selectAll('[data-link]');

            // Loop over all objets and find the next available iid.
            allIidSelection.each(function (){
                var feature = d3.select(this);
                var currentIid = parseInt(feature.attr('data-link'), 10);

                if (currentIid !== null && currentIid >= unusedIid) {
                    unusedIid = currentIid + 1;
                }
            });

            return unusedIid;
        }

        /**
         * Returns a universally unique identifier (UUID).
         *
         * @returns {String}
         *   A UUID
         */
        function getUUID() {
            var d = new Date().getTime();

            if(window.performance && typeof window.performance.now === "function") {
                d += performance.now(); //use high-precision timer if available
            }

            var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
                var r = (d + Math.random()*16)%16 | 0;
                d = Math.floor(d/16);

                return (c=='x' ? r : (r&0x3|0x8)).toString(16);
            });

            return uuid;
        }

        /**
         * Returns info on a file uploaded through an HTML input element.
         *
         * @param  {Object}
         *   Input file object that the user used to select the file.
         *
         * @returns {Promise}
         *   Promise. When it succeeds, it contains the following object:
         *   {
         *     'type': String,
         *     'dataUrl': String
         *   }
         */
        function getFileInfoFromInput(element) {
            var file = element.files[0],
                fileType = file.type,
                reader = new FileReader(),
                deferred = $q.defer();

            reader.readAsDataURL(file);
            reader.onload = function(e) {
                deferred.resolve({
                    type: fileType,
                    dataUrl: e.target.result,
                    file: file,
                });
            };

            reader.onerror = deferred.reject;

            return deferred.promise;
        }

    }

    angular.module(moduleApp).service('UtilService', UtilService);

    UtilService.$inject= ['$q'];

})();