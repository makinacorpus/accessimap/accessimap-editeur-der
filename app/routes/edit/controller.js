/*global turf, osmtogeojson */

/**
 * @ngdoc controller
 * @name accessimapEditeurDerApp.controller:EditController
 * @requires accessimapEditeurDerApp.EditService
 * @description
 * Controller of the '/edit' view
 *
 * The '/edit' view is a POC to leaflet's map & drawing
 *
 * This view allow the user to :
 * - display a map (thanks to leaflet)
 * - search some features (address, POI, areas)
 * - get data for POI or for a specific set (buildings, roads, ...) & display them
 * - visualize the area which will be print
 */
(function() {
    'use strict';

    function EditController(EditService, ToasterService, SelectPathService, HistoryService, UtilService, FileStoreService, InteractionService, $location, $q, $scope, $rootScope) {
        var $ctrl = this;

        /**
         * @ngdoc property
         * @name  queryChoices
         * @propertyOf accessimapEditeurDerApp.controller:EditController
         *
         * @description
         * Options of POI and area to add on the map
         */
        $ctrl.queryChoices = EditService.settings.QUERY_LIST;

        /**
         * @ngdoc property
         * @name  queryChosen
         * @propertyOf accessimapEditeurDerApp.controller:EditController
         *
         * @description
         * POI / area selected
         */
        $ctrl.queryChosen  = EditService.settings.QUERY_DEFAULT; // $ctrl.queryChoices[1];
        $ctrl.queryChosenPoi  = EditService.settings.QUERY_POI;

        /**
         * @ngdoc property
         * @name  styleChoices
         * @propertyOf accessimapEditeurDerApp.controller:EditController
         *
         * @description
         * Options of styling for the queryChosen' type
         */
        $ctrl.styleChoices = EditService.settings.STYLES[$ctrl.queryChosen.type];
        $ctrl.styleChoicesPoi = EditService.settings.STYLES[$ctrl.queryChosenPoi.type];


        /**
         * @ngdoc property
         * @name  pointChoices
         * @propertyOf accessimapEditeurDerApp.controller:EditController
         *
         * @description
         * Options of styling for POI / points
         */
        $ctrl.pointChoices = EditService.settings.STYLES.point;

        /**
         * @ngdoc property
         * @name  styleChosen
         * @propertyOf accessimapEditeurDerApp.controller:EditController
         * @description
         * Style selected for the queryChosen' type
         */
        $ctrl.styleChosen  = $ctrl.styleChoices[0];
        $ctrl.styleChosenPoi  = $ctrl.styleChoicesPoi[0];

        /**
         * @ngdoc
         * @name  changeStyle
         * @methodOf accessimapEditeurDerApp.controller:EditController
         * @description Update the styleChoices & styleChosen properties according to the queryChosen type
         */
        $ctrl.changeStyle  = function() {
            $ctrl.styleChoices = EditService.settings.STYLES[$ctrl.queryChosen.type];
            $ctrl.styleChosen  = $ctrl.styleChoices[0];
        };

        $ctrl.fonts                      = EditService.settings.FONTS;
        $ctrl.fontChosen                 = $ctrl.fonts[0];
        $ctrl.fontColors                 = EditService.settings.COLORS;
        $ctrl.fontColorChosen            = $ctrl.fontColors[$ctrl.fontChosen.color][0];

        $ctrl.colors                     = (EditService.settings.COLORS.transparent)
                                                .concat(EditService.settings.COLORS.other);

        $ctrl.colorChosen                = $ctrl.colors[0];
        $ctrl.featureIcon                = EditService.featureIcon;
        $ctrl.formats                    = EditService.settings.FORMATS;
        $ctrl.backgroundStyleChoices     = EditService.settings.STYLES.polygon;
        $ctrl.getFeatures                = EditService.getFeatures;


        // general state parameters
        $ctrl.expandedMenu                   = false;
        $ctrl.panel                 = null;

        $ctrl.poiMode = false;

        $ctrl.isFiltersPanelCollapsed = false;

        $ctrl.isDrawingFrozen = true;
        $ctrl.isBrailleDisplayed  = true;

        $ctrl.markerStartChoices  = EditService.settings.markerStart;
        $ctrl.markerStopChoices   = EditService.settings.markerStop;

        $ctrl.isUndoAvailable     = EditService.isUndoAvailable;

        $ctrl.interactionOpen     = {};

        // Keywords user types for filtering the list of POIs.
        $ctrl.poiFilterKeywords = '';

        $ctrl.initState = function() {
            $ctrl.mapFormat = $location.search().mapFormat
                            ? $location.search().mapFormat
                            : 'landscapeA4';
            $ctrl.legendFormat = $location.search().legendFormat
                            ? $location.search().legendFormat
                            : 'landscapeA4';

            $ctrl.checkboxModel = { contour: true};

            $ctrl.model = {
                title           : 'Nouveau dessin',
                isMapVisible    : false,
                comment         : 'Pas de commentaire',
                mapFormat       : 'landscapeA4',
                legendFormat    : 'landscapeA4',
                backgroundColor : $ctrl.colors[0], // transparent
                backgroundStyle : EditService.settings.STYLES.polygon[EditService.settings.STYLES.polygon.length - 1],
            }
        }

        $ctrl.init = function() {
            $ctrl.initState();
            EditService.init($ctrl.mapFormat, $ctrl.legendFormat);
        }

        $ctrl.expandMenu = function() {
            $ctrl.expandedMenu = $ctrl.expandedMenu ? false : true;
        }

        $ctrl.undo = function() {
            EditService.undo();

            // Re-activate current drawing mode so event handlers get re-attached
            // correctly to DOM elements.
            $ctrl.enableDrawingMode($ctrl.mode);
        }

        $ctrl.redo = function() {
            EditService.redo();

            // Re-activate current drawing mode so event handlers get re-attached
            // correctly to DOM elements.
            $ctrl.enableDrawingMode($ctrl.mode);
        }


        // Key events
        var onMoveFrame = false;
        $ctrl.moveFrame = function() {
            onMoveFrame = true
            EditService.enableDefaultMode();
            $ctrl.isDrawingFrozen = true;
            EditService.freezeMap();
        }
        function stopMoveFrame() {
            if (onMoveFrame) {
                $ctrl.enableDrawingMode($ctrl.mode);
                onMoveFrame = false;
            }
        }

        function KeyPress(e) {
            var evtobj = window.event? event : e
            if (evtobj.keyCode == 90 && evtobj.ctrlKey) $ctrl.undo(); // ctrl + Z
            if (evtobj.keyCode == 89 && evtobj.ctrlKey) $ctrl.redo(); // ctrl + Y
            if (evtobj.keyCode == 32 && $ctrl.mode !== 'select') $ctrl.moveFrame(); // Espace

            // Escape resets the edition state.
            if (evtobj.keyCode == 27) {
                $ctrl.resetFeature();
                $ctrl.enableDrawingMode('select');
                $scope.$apply();
            }
        }

        document.onkeydown = KeyPress;
        document.onkeyup = stopMoveFrame;

        $ctrl.reset = function() {
            if (window.confirm('En validant, vous allez effacer votre dessin en cours et en créer un nouveau.'))
                window.location.reload();
        }

        $ctrl.exportData = function() {
            SelectPathService.visuallyUnselectAllFeatures();

            // Clean drawing's interactions before exporting.
            InteractionService.removeAllEmptyInteractions();
            InteractionService.removeAllEmptyPOIs();

            ToasterService.info('Export du dessin en cours...\n' +
                'Merci de patienter', {timeout: 0, tapToDismiss: false})
            EditService.exportData($ctrl.model)
                .then(function () {
                    ToasterService.remove()
                    ToasterService.success('Export terminé !')
                })
                .catch(function(error) {
                    ToasterService.remove()
                    ToasterService.error(error, 'Erreur lors de l\'export...');
                });
        };
        $ctrl.rotateMap           = EditService.rotateMap;

        $ctrl.changeDrawingFormat = function(format) {
            EditService.changeDrawingFormat(format);
            EditService.updateBackgroundStyleAndColor($ctrl.model.backgroundStyle, $ctrl.model.backgroundColor);
        }
        $ctrl.changeLegendFormat  = EditService.changeLegendFormat;

        $ctrl.interactions        = EditService.interactions;

        $ctrl.mapCategories       = EditService.settings.mapCategories;

        $ctrl.importBackground    = EditService.importBackground;
        $ctrl.importImage         = EditService.importImage;
        $ctrl.appendSvg           = EditService.appendSvg;

        $ctrl.defaultFeatureProperties = {};

        $ctrl.importDER = function($event) {
            // Get file.
            var file = null;
            if (typeof $event.target == 'undefined') {
                return;
            }
            file = $event.target;

            // Confirm user wants to erase the whole drawing to replace it with an imported file.
            // Note: This action is undoable.
            var carryOnWithImport = confirm('Votre dessin actuel va être remplacé par le fichier à importer. Êtes-vous sûr ?');
            if (!carryOnWithImport) {
                // User cancelled the import. Abort.
                return;
            }

            // Restore the 'clear' state to clear out all existing shapes.
            HistoryService.restoreNamedState('clear');

            ToasterService.info('Import du fichier... merci de patienter', {timeout: 0, tapToDismiss: false})

            EditService.importDER(file)
                .then(function definedModel(model) {
                    ToasterService.remove()
                    ToasterService.success('Import terminé !')
                    $ctrl.model = model;

                    // Save the state now that the file was successfully imported.
                    HistoryService.saveState();
                })
                .catch(function(error) {
                    ToasterService.remove()
                    ToasterService.error(error, 'Erreur lors de l\'import...')
                });
        }

        /**
         * @ngdoc method
         * @name  showMap
         * @methodOf accessimapEditeurDerApp.controller:EditController
         *
         * @description
         * Show the map layer
         */
        $ctrl.showMap = function() {
            $ctrl.model.isMapVisible = true;
            EditService.showMapLayer();
        }

        /**
         * @ngdoc method
         * @name  hideMap
         * @methodOf accessimapEditeurDerApp.controller:EditController
         *
         * @description
         * Hide the map layer
         */
        $ctrl.hideMap = function() {
            $ctrl.model.isMapVisible = false;
            EditService.hideMapLayer()
        }

        /**
         * @ngdoc method
         * @name  toggleMap
         * @methodOf accessimapEditeurDerApp.controller:EditController
         *
         * @description
         * Toggle the map layer
         */
        $ctrl.toggleMap = function() {
            if ($ctrl.model.isMapVisible) {
                $ctrl.model.isMapVisible = false;
                EditService.hideMapLayer()
            } else {
                $ctrl.model.isMapVisible = true;
                EditService.showMapLayer();
            }
        }

        /**
         * @ngdoc method
         * @name  toggleFontBraille
         * @methodOf accessimapEditeurDerApp.controller:EditController
         *
         * @description
         * Toggle legend in braille / plain text
         */
        $ctrl.toggleFontBraille = function() {
            if ($ctrl.isBrailleDisplayed) {
                $ctrl.isBrailleDisplayed = false;
                EditService.hideFontBraille();
            } else {
                $ctrl.isBrailleDisplayed = true;
                EditService.showFontBraille();
            }
        }

        $ctrl.resetView = EditService.resetView;

        /**
         * Map parameters
         */
        $ctrl.displayAddPOIForm = function() {
            $ctrl.poiMode = true;
            EditService.enableAddPOI(ToasterService.warning, ToasterService.error, getPOIDrawingParameters);
        }

        $ctrl.initOSMMode = function() {
            EditService.initOSMMode();
        }

        $ctrl.insertOSMData = function()  {
            EditService.initOSMMode();
            EditService.insertOSMData($ctrl.queryChosen,
                                        ToasterService.warning,
                                        ToasterService.error,
                                        ToasterService.info,
                                        getDrawingParameters)
        }

        /**
         * General parameters
         */
        $ctrl.displayParameters = function() {
            EditService.resetState();

            if ($ctrl.panel === 'parameters') {
                $ctrl.panel = null;
                return false;
            }

            $ctrl.panel = 'parameters';
        }
        $ctrl.displayMapParameters = function() {
            EditService.resetState();

            if ($ctrl.panel === 'map') {
                $ctrl.panel = null;
                return false;
            }

            $ctrl.panel = 'map';
            $ctrl.initOSMMode();
            $ctrl.showMap();
        }
        $ctrl.displayDrawingParameters = function() {
            EditService.resetState();

            if ($ctrl.panel === 'draw') {
                $ctrl.panel = null;
                return false;
            }

            $ctrl.panel = 'draw';
            $ctrl.enableDrawingMode('select');

            $ctrl.isDrawingFrozen = true;
            EditService.freezeMap();
        }
        $ctrl.displayLegendParameters = function() {
            EditService.resetState();

            if ($ctrl.panel === 'legend') {
                $ctrl.panel = null;
                return false;
            }

            $ctrl.panel = 'legend';
        }
        $ctrl.displayInteractionParameters = function() {
            EditService.resetState();

            if ($ctrl.panel === 'interaction') {
                $ctrl.panel = null;
                return false;
            }

            $ctrl.panel = 'interaction';
            $ctrl.enableDrawingMode('select');
        }
        $ctrl.displayBackgroundParameters = function() {
        }

        $ctrl.removeFeature = EditService.removeFeature;
        $ctrl.updateFeature = EditService.updateFeature;
        $ctrl.rotateFeature = EditService.rotateFeature;

        $ctrl.updatePoint   = EditService.updatePoint;

        $ctrl.updateMarker  = EditService.updateMarker;

        $ctrl.updateColor = function(color) {
            EditService.updateFeatureStyleAndColor(null, color);
        }

        $ctrl.updateStyle = function(style) {
            EditService.updateFeatureStyleAndColor(style, null);
        }

        $ctrl.updateBackgroundColor = function(color) {
            EditService.updateBackgroundStyleAndColor($ctrl.model.backgroundStyle, color);
        }

        $ctrl.updateBackgroundStyle = function(style) {
            EditService.updateBackgroundStyleAndColor(style, $ctrl.model.backgroundColor);
        }

        /**
         * Returns the set of currently selected drawing parameters.
         *
         * @returns {Object}
         *   An object listing drawing parameters. Details:
         *   {
         *     style: Object. An entry from SettingsStyles.ALL_STYLES,
         *     color: Object. An entry from SettingsColors.ALL_COLORS,
         *     font: Object. An entry from SettingsFonts.FONTS,
         *     fontColor: Object. An entry from SettingsColors.ALL_COLORS,
         *     contour: Boolean. True if shape should be outlined.
         *     mode: String. Current drawing mode. See $ctrl.enableDrawingMode().
         *   }
         */
        function getDrawingParameters() {
            return {
                style: $ctrl.styleChosen,
                color: $ctrl.colorChosen,
                font: $ctrl.fontChosen,
                fontColor: $ctrl.fontColorChosen,
                contour: $ctrl.checkboxModel ? $ctrl.checkboxModel.contour : false,
                mode: $ctrl.mode
            }
        }

        /**
         * Returns the set of currently selected drawing parameters for POIs.
         * Similar to getDrawingParameters() but retrieves the 'style' property from
         * the "Add a POI" form instead.
         *
         * @returns {Object}
         *   An object listing style properties for POIs.
         *   See getDrawingParameters() for further details.
         */
        function getPOIDrawingParameters() {
            var parameters = getDrawingParameters();
            parameters.style = $ctrl.styleChosenPoi;

            return parameters;
        }

        $ctrl.resetFeature = function() {
            SelectPathService.visuallyUnselectAllFeatures();
            $ctrl.featureProperties = null;
            $ctrl.currentFeature = null;
        }

        // switch of editor's mode
        // adapt user's interactions
        $ctrl.properties = EditService.properties ;
        $ctrl.filterProperty = function(currentProperty) {
            return currentProperty.visible === true
                && ( currentProperty.type === 'all'
                    || currentProperty.type.indexOf($ctrl.featureProperties['data-type']) >= 0 );
        }

        /**
         * Called when properties of a Feature got changed.
         */
        $ctrl.onFeaturePropertiesChange = function() {
            EditService.setProperties($ctrl.currentFeature, $ctrl.featureProperties);
            EditService.historySave();
        }

        function selectInteraction(poiId) {
            Object.keys($ctrl.interactionOpen).map(function(key) {
                $ctrl.interactionOpen[key] = false;
            });
            $ctrl.interactionOpen['poi-' + poiId] = true;
        }

        $ctrl.enableDrawingMode = function(mode) {
            EditService.resetState();

            $ctrl.mode = mode;

            function setStyles(styleSetting) {
                $ctrl.styleChoices = EditService.settings.STYLES[styleSetting];
                $ctrl.styleChosen  = $ctrl.styleChoices[0];
            }

            function setFeatureProperties(feature) {
                var featureProperties = EditService.getProperties(feature);
                featureProperties.interactions = EditService.getInteractionByFeature(feature);
                $ctrl.featureProperties = featureProperties;
                $ctrl.currentFeature = feature;

                // Open interaction for given feature, and create it if it does
                // not exist yet.
                var featureIid = $ctrl.openInteraction(feature);

                // Select (open) this interaction in the Interaction panel on screen.
                $ctrl.currentPoi = featureIid;
                selectInteraction(featureIid);

                $scope.$apply();
            }

            $ctrl.resetFeature();

            switch ($ctrl.mode) {

                // case 'default':
                //     EditService.enableDefaultMode();
                //     break;

                case 'select':
                    EditService.enableSelectMode(setFeatureProperties);
                    break;

                case 'point':
                    setStyles($ctrl.mode);
                    EditService.enablePointMode(getDrawingParameters);
                    break;

                case 'circle':
                    setStyles('polygon');
                    EditService.enableCircleMode(getDrawingParameters);
                    break;

                case 'square':
                    setStyles('polygon');
                    EditService.enableSquareMode(getDrawingParameters);
                    break;

                case 'triangle':
                    setStyles('polygon');
                    EditService.enableTriangleMode(getDrawingParameters);
                    break;

                case 'line':
                case 'polygon':
                    setStyles($ctrl.mode);
                    EditService.enableLineOrPolygonMode(getDrawingParameters);
                    break;

                case 'addtext':
                    EditService.enableTextMode(getDrawingParameters);
                    break;

                case 'image':
                    EditService.enableImageMode(getDrawingParameters);
                    break;
            }

        }

        $ctrl.searchAddress      = function() {

            var promises = [];

            if($ctrl.address.start) {
                promises.push(
                    EditService.searchAndDisplayAddress($ctrl.address.start,
                                        'startPoint',
                                        'Point de départ',
                                        $ctrl.styleChosen,
                                        $ctrl.colorChosen));
            }

            if($ctrl.address.stop) {
                promises.push(
                    EditService.searchAndDisplayAddress($ctrl.address.stop,
                                        'stopPoint',
                                        'Point d\'arrivée',
                                        $ctrl.styleChosen,
                                        $ctrl.colorChosen));
            }

            // center the map or display both of addresses
            $q.all(promises)
                .then(function(results) {
                    if (results.length > 1) {
                        // fitBounds
                        EditService.fitBounds([
                                [results[0].lat, results[0].lon],
                                [results[1].lat, results[1].lon],
                            ])
                    } else {
                        // pan
                        EditService.panTo([results[0].lat, results[0].lon])
                    }
                })
                .catch(function(error) {
                    ToasterService.error(error);
                })
        };

        /**
         * Opens up the interaction properties for given Feature.
         * If corresponding interaction does not exist yet, add sit to the list and
         * attributes an Iid to given Feature if need be.
         *
         * @param {Object} feature
         *   The feature to open interactions for.
         *
         * @returns {int}
         *   The Iid (interaction ID) for this feature, as it may have been generated on the fly.
         */
        $ctrl.openInteraction = function(feature) {
            // If provided feature does not have a Iid yet, attribute a new one to it.
            var featureIid = feature.attr('data-link');
            if (featureIid === null) {
                featureIid = UtilService.getNextUnusedIid();
                feature.attr('data-link', featureIid);
            }

            // Retrieve feature's name if any is defined.
            var featureName = feature.attr('name');

            // If no interaction exists yet for this feature, add it.
            if (EditService.interactions.getInteractionByFeature(feature) === null) {
                $ctrl.addInteraction('poi-' + featureIid, null, null, featureName);
            }

            // Clear all POIs & interactions that are empty.
            clearEmptyPOIsAndInteractions('poi-' + featureIid);

            return featureIid;
        }

        $ctrl.goToInteraction = function() {
            $ctrl.displayInteractionParameters();

            var feature = getFeatureFromIid($ctrl.currentPoi);

            if (feature !== null) {
                setTimeout(function() {
                    $ctrl.openInteraction(feature);
                }, 300);
            }
        }

        $ctrl.addInteraction = function(poiIndex, label, filterId, interactionValue) {
            EditService.interactions.addInteraction(poiIndex, label, filterId, interactionValue);

            $ctrl.onInteractionPropertiesChange();
        }

        /**
         * Removes an interaction.
         *
         * @param {String} uuid
         *   UUID of the interaction to remove.
         */
        $ctrl.removeInteractionByUUID = function(uuid) {
            EditService.interactions.removeInteractionByUUID(uuid);

            // Update currently selected feature's properties if any.
            if ($ctrl.currentFeature && $ctrl.featureProperties) {
                $ctrl.featureProperties.interactions = EditService.getInteractionByFeature($ctrl.currentFeature);
            }

            $ctrl.onInteractionPropertiesChange();
        }

        /**
         * Adds a filter and saves current state.
         * See InteractionService.addFilter() for parameters.
         */
        $ctrl.addInteractionFilter = function(name, id) {
            // Add filter.
            $ctrl.interactions.addFilter(name, id);

            $ctrl.onInteractionPropertiesChange();
        }

        /**
         * Removes a filter and saves current state.
         * See InteractionService.removeFilter() for parameters.
         */
        $ctrl.removeInteractionFilter = function(id) {
            // Remove filter.
            $ctrl.interactions.removeFilter(id);

            $ctrl.onInteractionPropertiesChange();
        }

        /**
         * Imports an audio file and attaches it to given interaction.
         */
        $ctrl.importInteractionAudio = function($event, interaction) {
            // Get file input.
            var fileInput = null;
            if (typeof $event.target == 'undefined') {
                return;
            }
            fileInput = $event.target;

            UtilService.getFileInfoFromInput(fileInput)
                .then(function(fileInfo) {
                    EditService.importInteractionAudio(fileInfo.file, interaction);
                    $ctrl.onInteractionPropertiesChange();
                });
        }

        /**
         * Variables and methods handling Interations' audio files previews.
         */
        var audioPreviewObject = new Audio();
        $ctrl.interactionAudioPreviewPlaying = false;
        audioPreviewObject.addEventListener('play', function (e) {
            $ctrl.interactionAudioPreviewPlaying = true;
            // This forces AngularJS to update the interface, so the play button's
            // state reflects the current audio playback.
            $scope.$apply();
        });
        audioPreviewObject.addEventListener('pause', function (e) {
            $ctrl.interactionAudioPreviewPlaying = false;
            // This forces AngularJS to update the interface, so the play button's
            // state reflects the current audio playback.
            $scope.$apply();
        });

        $ctrl.playInteractionSound = function(interaction) {
            var fileInfo = FileStoreService.getFileByUniqueName(interaction.value);

            if (fileInfo === null) {
                return;
            }

            var blobUrl = URL.createObjectURL(fileInfo.file);
            audioPreviewObject.src = blobUrl;
            audioPreviewObject.play();
        }

        $ctrl.stopInteractionSound = function() {
            audioPreviewObject.pause();
        }

        /**
         * Actions to be done whenever interaction properties get changed:
         * - Update interactions' validity states.
         * - Saves history state.
         */
        $ctrl.onInteractionPropertiesChange = function() {
            // Check validity state of all interactions.
            $ctrl.updateAllInteractionsValidityStates();

            // Save current state.
            HistoryService.saveState();
        }

        $ctrl.onInteractionProtocolChange = function(interaction) {
            // When user switches between protocols, reset the value field as it may contain
            // other protocols' values.
            interaction.value = '';

            $ctrl.onInteractionPropertiesChange();
        }

        /**
         * Duplicates an interaction to all filters of given POI.
         */
        $ctrl.duplicateInteractionToAllFilters = function(poiId, interaction) {
          var poi = InteractionService.getPoiFromId(poiId);
          var poiLabel = (poi.label) ? poi.label + ' ' : '';
          poiLabel += '(' + poi.id + ')';

          var interactionType = interaction.gesture;

          var confirmRes = confirm(
            "Vous êtes sur le point de dupliquer l'interaction de type '" + interactionType + "' sur " +
            "tous les filtres du POI '" + poiLabel + "'. Si des interactions de ce type existent déjà " +
            "sur les autre filtres, elles seront remplacées. \n\n" +
            "Continuer ?"
          );

          // User declined? Abort.
          if (!confirmRes) {
            return;
          }

          InteractionService.duplicateInteractionToAllFilters(poiId, interaction);
        }

        $ctrl.getInteractionsByFilterLength = function(interactions, filterId) {
            var interactionFiltered = interactions.filter(function(interaction) {
                return interaction.filter === filterId;
            });
            return interactionFiltered.length;
        }

        // List of interaction audio files.
        $ctrl.interactionAudioFileList = [{
            id: '',
            label: '- Aucun fichier choisi -',
        }];
        // Register to FileStore changes so we can update the list of available audio files.
        FileStoreService.onFileStoreChange(function() {
            var allFiles = FileStoreService.getAllFiles(true);
            $ctrl.interactionAudioFileList = [{
                id: '',
                label: '- Aucun fichier choisi -',
            }];

            // Loop over files to build the list of options.
            for (var i = 0; i < allFiles.length; i++) {
                var fileInfo = allFiles[i];
                $ctrl.interactionAudioFileList.push({
                    id: fileInfo.unique_filename,
                    label: fileInfo.unique_filename,
                });
            }
        });

        $ctrl.updateAllInteractionsValidityStates = function(poi) {
            EditService.interactions.updateAllInteractionsValidityStates();
        }

        $ctrl.highlightFeatureFromPoi = function(poi) {
            var feature = selectFeatureByPoiId(poi.id);

            if (feature !== null) {
                SelectPathService.highlightFeature(feature);
            }
        }

        $ctrl.unlightFeatureFromPoi = function(poi) {
            var feature = selectFeatureByPoiId(poi.id);

            if (feature !== null) {
                SelectPathService.unlightFeature(feature);
            }
        }

        /**
         * Triggered when user clicks on a POI accordion.
         * Visually highlights corresponding feature on the drawing.
         *
         * @param {Object} poi
         */
        $ctrl.visuallySelectFeatureFromPoi = function(poi) {
            var feature = selectFeatureByPoiId(poi.id);

            if (feature !== null) {
                SelectPathService.visuallyUnselectAllFeatures();
                SelectPathService.visuallySelectFeature(feature);
            }

            // Clear all POIs & interactions that are empty.
            clearEmptyPOIsAndInteractions(poi.id);

            // Also select the poi as being the current one.
            this.currentPoi = getFeatureIidFromPoiId(poi.id);
        }

        /**
         * Returns true if given POI should be visible, in step with
         * current user's typed keywords.
         *
         * @param {Object} poi
         *   The POI to test.
         * @returns bool.
         *   True if given POI should be visible. False otherwise.
         */
        $ctrl.shouldFilterIncludePoi = function(poi) {
          // If given POI is the current one, we should always
          // display it.
          if (poi.id == 'poi-' + $ctrl.currentPoi) {
            return true;
          }

          // Gather all keywords to be searched.
          var keywords = $ctrl.poiFilterKeywords.split(' ');

          // Loop over all keywords to search.
          for (var i = 0; i < keywords.length; i++) {
            var keyword = keywords[i];

            if (!doesPoiMatchKeyword(poi, keyword)) {
              // One of the keywords does not match: return false
              // right away.
              return false;
            }
          }

          // All keywords matched: return true.
          return true;
        }

        /**
         * Helper clearing all empty interactions and POIs that do not
         * have any interactions.
         * @param {String} exceptionPoiId
         *   If a POI should be excluded from this cleaning process.
         *   Should be POI's string ID (e.g.: 'poi-3')
         */
        function clearEmptyPOIsAndInteractions(exceptionPoiId) {
          // Clear all POIs & interactions that are empty.
          InteractionService.removeAllEmptyInteractions(exceptionPoiId);
          InteractionService.removeAllEmptyPOIs(exceptionPoiId);
        }
    }

    /**
     * Determines if given POI matches given keyword (case insensitive).
     *
     * @param {Object} poi
     *   The POI to test.
     * @param {String} keyword
     *   A single keyword to search in the POI's string properties.
     * @returns Bool
     *   True if given POI should be visible. False otherwise.
     */
    function doesPoiMatchKeyword(poi, keyword) {
      // If keyword is empty, consider it's a match.
      if (keyword === '') {
        return true;
      }

      keyword = String(keyword.toLowerCase());

      // Gather all parts we're gonna look into.
      var stringsArray = [];
      if (typeof poi.label !== 'undefined' && poi.label !== null) {
        stringsArray.push(String(poi.label));
      }
      if (typeof poi.id !== 'undefined' && poi.id !== null) {
        stringsArray.push(String(poi.id));
      }
      if (typeof poi.value !== 'undefined' && poi.value !== null) {
        stringsArray.push(String(poi.value));
      }

      // Loop over all strings to search in.
      for (var i = 0; i < stringsArray.length; i++) {
        var currentString = stringsArray[i].toLowerCase();

        if (currentString.indexOf(keyword) > -1) {
          // A match was found, return right away.
          return true;
        }
      }

      // No match found. Return false.
      return false;
    }

    /**
     * Helper returning the Iid of a feature matching given POI's ID.
     *
     * @param {string} poiId
     *   POI's ID (for instance: "poi-7").
     *
     * @returns {string}
     *   Matching Feature ID. For instance: "7".
     *   If given POI ID was invalid, returns null.
     */
    function getFeatureIidFromPoiId(poiId) {
        var poiIdParts = poiId.split('-');

        if (poiIdParts.length >= 2) {
            return poiIdParts[1]
        }
        else {
            return null;
        }
    }

    /**
     * Helper returning a Feature object based on a POI's ID.
     *
     * @param {string} poiId
     *   POI's ID (for instance: "poi-7").
     *
     * @returns {Object}
     *   The matching feature. NULL if not found.
     */
    function selectFeatureByPoiId(poiId) {
        var featureIid = getFeatureIidFromPoiId(poiId);
        return d3.select('[data-link="' + featureIid + '"]');
    }

    function getFeatureFromIid(iid) {
        return d3.select('[data-link="' + iid + '"]');
    }

    angular.module(moduleApp).controller('EditController', EditController);

    EditController.$inject = [
        'EditService',
        'ToasterService',
        'SelectPathService',
        'HistoryService',
        'UtilService',
        'FileStoreService',
        'InteractionService',
        '$location',
        '$q',
        '$scope',
        '$rootScope',
    ];
})();
