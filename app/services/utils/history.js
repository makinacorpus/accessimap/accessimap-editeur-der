/**
 * @ngdoc service
 * @name accessimapEditeurDerApp.HistoryService
 *
 * @description
 * Service exposing history functions to allow user to roll back
 */
(function() {
    'use strict';

    function HistoryService($rootScope, InteractionService) {
        this.init            = init;
        this.saveState       = saveState;
        this.undoState       = undoState;
        this.redoState       = redoState;
        this.enableUndoRedo  = enableUndoRedo;
        this.disableUndoRedo = disableUndoRedo;

        this.restoreNamedState  = restoreNamedState;

        // List of saved states as part of the history.
        var historyUndo = [];
        var historyRedo = [];

        // List of states that got named so they are persistent.
        var namedStates = {};

        var maxHistorySize = 20;
        var svgDrawing,
            applyStyle,
            d = {
                imageLayer: null,
                polygonsLayer: null,
                linesLayer: null,
                pointsLayer: null,
                textsLayer: null,
            },
            g = {
                imageLayer: null,
                polygonsLayer: null,
                linesLayer: null,
                pointsLayer: null,
                textsLayer: null,
            };

        var undoRedoEnabled = true;

        function init(_svgDrawing) {
            svgDrawing = _svgDrawing;

            // Draw elements
            d.imageLayer = svgDrawing.select('#drawing-layer [data-name="images-layer"]')[0][0];
            d.polygonsLayer = svgDrawing.select('#drawing-layer [data-name="polygons-layer"]')[0][0];
            d.linesLayer = svgDrawing.select('#drawing-layer [data-name="lines-layer"]')[0][0];
            d.pointsLayer = svgDrawing.select('#drawing-layer [data-name="points-layer"]')[0][0];
            d.textsLayer = svgDrawing.select('#drawing-layer [data-name="texts-layer"]')[0][0];

            // Geojson elements
            g.polygonsLayer = document.querySelector('#geojson-layer [data-name="polygons-layer"]');
            g.linesLayer = document.querySelector('#geojson-layer [data-name="lines-layer"]');
            g.pointsLayer = document.querySelector('#geojson-layer [data-name="points-layer"]');
            g.textsLayer = document.querySelector('#geojson-layer [data-name="texts-layer"]');
        }

        function isDrawingLayerValid() {
            return d.imageLayer && d.polygonsLayer && d.linesLayer && d.pointsLayer && d.textsLayer
        }

        function isGeojsonLayerValid() {
            return g.polygonsLayer && g.linesLayer && g.pointsLayer && g.textsLayer
        }

        /**
         * Refreshes the rootScope hsitory states in step with current history status.
         */
        function refreshRootScope() {
            if (historyUndo.length > maxHistorySize) {
                historyUndo.shift();
            }

            if (undoRedoEnabled) {
                $rootScope.isUndoable = historyUndo.length > 1;
                $rootScope.isRedoable = historyRedo.length > 0;
            }
            else {
                $rootScope.isUndoable = false;
                $rootScope.isRedoable = false;
            }

            if (!$rootScope.$$phase) {
                $rootScope.$digest();
            }
        }

        /**
         * Flags undo & redo features as enabled. Only used for
         * changing interface undo/redo buttons (does not
         * prevent from undoing/redoing programmatically).
         */
        function enableUndoRedo() {
            undoRedoEnabled = true;
            refreshRootScope();
        }

        /**
         * Flags undo & redo features as disabled. Only used for
         * changing interface undo/redo buttons (does not
         * prevent from undoing/redoing programmatically).
         */
        function disableUndoRedo() {
            undoRedoEnabled = false;
            refreshRootScope();
        }

        function getLayersInnerHtml(group) {
            var layers = {
                polygonsLayer: group.polygonsLayer.innerHTML,
                linesLayer: group.linesLayer.innerHTML,
                pointsLayer: group.pointsLayer.innerHTML,
                textsLayer: group.textsLayer.innerHTML,
            };

            if (group.imageLayer) {
                layers.imageLayer = group.imageLayer.innerHTML;
            }

            return layers;
        }

        function updateDOMelement(group, newState) {
            if (newState) {
                if (group.imageLayer) {
                    group.imageLayer.innerHTML = newState.imageLayer;
                }
                group.polygonsLayer.innerHTML = newState.polygonsLayer;
                group.linesLayer.innerHTML = newState.linesLayer;
                group.pointsLayer.innerHTML = newState.pointsLayer;
                group.textsLayer.innerHTML = newState.textsLayer;
            }
        }

        /**
         * Gets the current state of the drawing.
         *
         * @returns mixed
         *   An object containg the current state of the drawing.
         *   Structure:
         *   {
         *     d: The drawing layer or null if invalid,
         *     g: The GeoJson layer or null if invalid,
         *     i: Interaction properties,
         *     f: Filter properties,
         *   }
         *   Returns null if the current state is not valid.
         */
        function getCurrentState() {
            var state = {d: null, g: null, i: null, f: null};
            var stateIsValid = false;

            // Get drawing layer.
            if (isDrawingLayerValid()) {
                state.d = getLayersInnerHtml(d);
                stateIsValid = true;
            }

            // Get GeoJson layer.
            if (isGeojsonLayerValid()) {
                state.g = getLayersInnerHtml(g);
                stateIsValid = true;
            }

            // Get Interactions and filters.
            state.i = JSON.stringify(InteractionService.getInteractions());
            state.f = JSON.stringify(InteractionService.getFilters());

            if (stateIsValid) {
                return state;
            }
            else {
                return null;
            }
        }

        /**
         * Applies given state to the DOM, restoring the drawing to a specific state.
         *
         * @param {Object} state
         *   The state to restore to the DOM, as returned by getCurrentState().
         */
        function restoreState(state) {
            if (state !== null && typeof state !== 'undefined') {
                // Restore drawing layer.
                if (state.d !== null && typeof state.d !== 'undefined') {
                    updateDOMelement(d, state.d);
                }
                // Restore GeoJson layer.
                if (state.g !== null && typeof state.g !== 'undefined') {
                    updateDOMelement(g, state.g);
                }
                // Restore interactions and filters.
                if (state.i !== null && typeof state.i !== 'undefined') {
                    InteractionService.setInteractions(JSON.parse(state.i));
                }
                if (state.f !== null && typeof state.f !== 'undefined') {
                    InteractionService.setFilters(JSON.parse(state.f));
                }
            }
        }

        /**
         * Retrieves a named state.
         *
         * @param {string} stateName
         *   Name of the state to retrieve.
         *
         * @returns {Object}
         *   The corresponding named state. Null if none could be found for given name.
         */
        function getNamedState(stateName) {
            if (typeof namedStates[stateName] !== 'undefined') {
                return namedStates[stateName];
            }
            else {
                return null;
            }
        }

        /**
         * Saves a named state in memory.
         *
         * @param {string} stateName
         *   Name to attribute to the state to save.
         * @param {Object} state
         *   The state to save.
         */
        function setNamedState(stateName, state) {
            namedStates[stateName] = state;
        }

        /**
         * Saves current state in the undo history.
         * Also clears the redo history.
         *
         * @param {string} stateName
         *   Optional. Identifier for finding this state back later.
         *   If this parameter is given, this state will persist in a separate space in memory so you can
         *   restore it later with restoreNamedState() even though it might have been dropped from history.
         *   If given name is already in use, it will overwrite the existing saved state that has this name.
         */
        function saveState(stateName) {
            var currentState = getCurrentState();

            if (currentState !== null) {
                historyUndo.push(currentState);
                historyRedo = [];

                // If a name was given, save this state aside.
                if (typeof stateName !== 'undefined') {
                    setNamedState(stateName, currentState);
                }
            }

            // Refresh undo/redo state.
            refreshRootScope();
        }

        /**
         * Restores a state base on its name that got saved previously by passing
         * a name to saveState().
         * This function does not add the restored state to the undo history.
         * Do it manually by calling saveState() if necessary.
         *
         * @param {string} stateName
         *   Name of the state to restore.
         *   If name could not be found, this function has no effect.
         */
        function restoreNamedState(stateName) {
            var stateToRestore = getNamedState(stateName);

            if (stateToRestore !== null) {
                // Restore named state.
                restoreState(stateToRestore);
            }
        }

        /**
         * Restores previous saved state from history to the DOM.
         */
        function undoState() {
            var currentState = getCurrentState();

            if (currentState !== null) {
                // Save current state instead of the state from undo history just in
                // case something untracked changed.
                historyRedo.push(currentState);
            }

            // Remove previous state from undo history.
            historyUndo.pop();

            // Restore DOM with previous state.
            if (historyUndo.length > 0) {
                var indexToRestore = historyUndo.length - 1;
                restoreState(historyUndo[indexToRestore]);
            }

            refreshRootScope();
        }

        /**
         * Restores next saved state from history to the DOM.
         */
        function redoState() {
            if (historyRedo.length == 0) {
                // Nothing to redo? Abort.
                return;
            }

            var currentState = getCurrentState();

            if (currentState !== null) {
                // Remove next state from redo history.
                var stateToRedo = historyRedo.pop();

                // Replace last state with current one just in case something untracked changed.
                historyUndo.pop();
                historyUndo.push(currentState);

                // Add to history the state being restored.
                historyUndo.push(stateToRedo);

                // Restore DOM with next state.
                restoreState(stateToRedo);
            }

            refreshRootScope();
        }
    }

    angular.module(moduleApp)
        .service('HistoryService', HistoryService);

    HistoryService.$inject = ['$rootScope', 'InteractionService']
})();