"use strict";

describe("Service: UtilService", function() {
  beforeEach(module("accessimapEditeurDerApp"));

  var UtilService, $rootScope;

  beforeEach(
    inject(function(_UtilService_, _$rootScope_) {
      UtilService = _UtilService_;
      $rootScope = _$rootScope_;
    })
  );

  it("should create the UtilService service", function() {
    expect(UtilService).toBeDefined();
    expect(UtilService.convertImgToBase64).toBeDefined();
    expect(UtilService.getNextUnusedIid).toBeDefined();
  });

  it("should return a iid unused at first", function() {
    expect(UtilService.getNextUnusedIid()).toBe(2);
  });

  it("should return a iid unused while it is unused", function() {
    expect(UtilService.getNextUnusedIid()).toBe(2);
    expect(UtilService.getNextUnusedIid()).toBe(2);
  });

  it("should convert a png image into a base64", function() {
    //TODO: test convertImgToBase64 by mocking image loading ?...
    UtilService.convertImgToBase64(null);
  });
});
