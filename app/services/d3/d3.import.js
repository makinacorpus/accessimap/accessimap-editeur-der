(function() {
    'use strict';

    /**
     * @ngdoc service
     * @name accessimapEditeurDerApp.ImportService
     * @memberOf accessimapEditeurDerApp
     *
     * @description
     * Provide service to import a drawing :
     * - background layer
     * - geojson layer
     * - drawing layer
     * - interactions
     * - legend
     */
    function ImportService(LayerService, InteractionService, LegendService, SettingsService, SVGService) {

        this.importDrawing      = importDrawing;
        this.importInteractions = importInteractions;
        this.getModelFromSVG    = getModelFromSVG;

        function isVersionOfSVGAcceptable(svgElement) {

            return svgElement.querySelector('svg').getAttribute('data-version') >= '0.1';

        }

        function getModelFromSVG(svgElement) {

            var model = null;

            if (isVersionOfSVGAcceptable(svgElement)) {
                var metadataModel = svgElement.querySelector('metadata[data-name="data-model"]');

                if (metadataModel) {
                    model = JSON.parse(metadataModel.getAttribute('data-value'));
                }
            }

            if (! model) {
                model = {
                    title           : 'Nouveau dessin',
                    isMapVisible    : false,
                    comment         : 'Pas de commentaire',
                    mapFormat       : 'landscapeA4',
                    legendFormat    : 'landscapeA4',
                    backgroundColor : SettingsService.COLORS.transparent[0], // transparent
                    backgroundStyle : SettingsService.STYLES.polygon[SettingsService.STYLES.polygon.length - 1],
                }
            }

            return model;
        }

        function cloneChildrenFromNodeAToB(nodeFrom, nodeTo, translationToApply) {

            var children = nodeFrom.children,
            // Retrieve all tags representing a visual element/shape that needs to be transformed.
                paths = nodeFrom.querySelectorAll('circle,ellipse,image,line,mesh,path,polygon,polyline,rect,text,use');

            if (translationToApply) {
                for (var i = 0; i < paths.length; i++) {
                    var currentD = paths[i].getAttribute('d');

                    if (currentD) {
                        var currentParseD = SVGService.parseSVGPath(currentD),
                            currentTranslateD = SVGService.translateSVGPath(currentParseD,
                                                                                translationToApply.x,
                                                                                translationToApply.y),
                            currentSerializeD = SVGService.serializeSVGPath(currentTranslateD);

                        paths[i].setAttribute('d', currentSerializeD)
                    } else {
                        var cx = paths[i].getAttribute('cx'),
                            cy = paths[i].getAttribute('cy'),
                            x = paths[i].getAttribute('x'),
                            y = paths[i].getAttribute('y');

                        if (cx !== null ) {
                            paths[i].setAttribute('cx', parseFloat(cx) + translationToApply.x)
                            paths[i].setAttribute('cy', parseFloat(cy) + translationToApply.y)
                        } else {
                            paths[i].setAttribute('x', parseFloat(x) + translationToApply.x)
                            paths[i].setAttribute('y', parseFloat(y) + translationToApply.y)
                        }

                    }
                }
            }

            for (var i = 0; i < children.length; i++) {
                var childrenDataName = children[i].getAttribute('data-name');
                if (childrenDataName !== null) {
                    var existingTargetDataNameElement = nodeTo.querySelector('[data-name="' + childrenDataName + '"]');

                    // If the target element already exists, simply replace its content as we do not
                    // want to duplicate it.
                    if (existingTargetDataNameElement !== null) {
                        existingTargetDataNameElement.innerHTML = children[i].innerHTML;
                        continue;
                    }
                }

                // If we reach this line, fallback to appending the element.
                nodeTo.appendChild(children[i]);
            }
        }

        /**
         * @ngdoc method
         * @name  importDrawing
         * @methodOf accessimapEditeurDerApp.ImportService
         *
         * @description
         * Import data from the svgElement by trying to find the layers
         *
         * @param  {DOM Element} svgElement
         * the element to import for drawing purpose
         */
        function importDrawing(svgElement) {
            var

            currentGeoJSONLayer    = LayerService.geojson.getLayer().node(),
            currentDrawingLayer    = LayerService.drawing.getLayer().node(),
            currentBackgroundLayer = LayerService.background.getLayer().node(),

            geojsonLayer    = svgElement.querySelector('g[data-name="geojson-layer"]'),
            drawingLayer    = svgElement.querySelector('g[data-name="drawing-layer"]'),
            backgroundLayer = svgElement.querySelector('g[data-name="background-layer"]'),
            overlayLayer    = svgElement.querySelector('svg[data-name="overlay"]'),
            marginLayer     = svgElement.querySelector('g[id="margin-layer"]'),

            metadataGeoJSON      = svgElement.querySelector('metadata[data-name="data-geojson"]'),

            currentOverlayRelativeTranslation = LayerService.overlay.getRelativeTranslation(),

            translateScaleOverlayGroup = ( overlayLayer === null )
                                        ? null
                                        : overlayLayer.getAttribute('transform'),

            translateOverlayGroup = ( translateScaleOverlayGroup === null )
                                    ? null
                                    : translateScaleOverlayGroup
                                        .substring(translateScaleOverlayGroup.indexOf('(') + 1,
                                                   translateScaleOverlayGroup.indexOf(')')),

            translateOverlayGroupArray = ( translateOverlayGroup === null )
                                         ? [0, 0]
                                         : translateOverlayGroup.slice(0, translateOverlayGroup.length).split(','),

            translateMarginGroup = ( marginLayer === null )
                                ? null
                                : marginLayer.getAttribute('transform'),

            translateMargin = ( translateMarginGroup === null )
                            ? null
                            : translateMarginGroup
                                .substring(translateMarginGroup.indexOf('(') + 1,
                                           translateMarginGroup.indexOf(')')),

            translateMarginArray = ( translateMargin === null )
                                   ? [0, 0]
                                   : translateMargin.slice(0, translateMargin.length).split(','),

            translationToApply = { x: currentOverlayRelativeTranslation.x
                                     - translateOverlayGroupArray[0]
                                     - translateMarginArray[0],
                                   y: currentOverlayRelativeTranslation.y
                                     - translateOverlayGroupArray[1]
                                     - translateMarginArray[1]
                                 };

            if (isVersionOfSVGAcceptable(svgElement)) {

                // if exists, inserts data of the geojson layers
                if (geojsonLayer) {
                    cloneChildrenFromNodeAToB(geojsonLayer, currentGeoJSONLayer, translationToApply);
                }

                // if exists, inserts data of the drawing layers
                if (drawingLayer) {
                    cloneChildrenFromNodeAToB(drawingLayer, currentDrawingLayer, translationToApply);
                }

                // if exists, inserts data of the background layers
                if (backgroundLayer) {
                    cloneChildrenFromNodeAToB(backgroundLayer, currentBackgroundLayer, translationToApply);
                }

                if (metadataGeoJSON && metadataGeoJSON.getAttribute('data-value') !== '') {
                    var dataGeoJSON = JSON.parse(metadataGeoJSON.getAttribute('data-value'));
                    LayerService.geojson.setFeatures(dataGeoJSON);
                    generateLegend(dataGeoJSON);
                }

            } else {
                // SVG file being imported is not a drawing from the DER.
                // Apply some transformations so it matches our canvas size & position.
                var transformationsToApply = '';

                // Apply a translation so the imported SVG is correctly positionned.
                transformationsToApply += 'translate(' + translationToApply.x + ',' + translationToApply.y + ')';

                // Compute the scale ratio to apply to the entire SVG being imported so
                // its elements have the right size within our context, despite their viewbox
                // that might not match ours.
                var svg = svgElement.querySelector('svg');
                // Get SVG size as computed by the browser based on its properties.
                var svgWidth = svg.width.baseVal.value;
                var svgHeight = svg.height.baseVal.value;
                // Get SVG viewbox size.
                var svgViewBoxWidth = svg.viewBox.baseVal ? svg.viewBox.baseVal.width : null;
                var svgViewBoxHeight = svg.viewBox.baseVal ? svg.viewBox.baseVal.height : null;
                if (svgViewBoxWidth === 0) {
                  svgViewBoxWidth = null;
                }
                if (svgViewBoxHeight === 0) {
                  svgViewBoxHeight = null;
                }
                // Compute X and Y scale ratios to apply.
                var ratioX = (svgViewBoxWidth === null) ? 1 : svgWidth / svgViewBoxWidth;
                var ratioY = (svgViewBoxHeight === null) ? 1 : svgHeight / svgViewBoxHeight;
                transformationsToApply += ' scale(' + ratioX + ',' + ratioY + ')';

                // Remove some elements that crash the export.
                var metadata = svgElement.querySelector('metadata');
                if (metadata) {
                    metadata.remove();
                }

                var namedview = svgElement.querySelector('namedview');
                if (namedview) {
                    namedview.remove();
                }

                // What follows is linked to a bug when exporting a DERi with 'perspective'
                // information from Inkscape.
                var inkscapePerspectiveElement = svgElement.querySelector('perspective');
                if (inkscapePerspectiveElement) {
                    inkscapePerspectiveElement.remove();
                }

                // Append result to the 'drawing section'.
                LayerService.drawing.appendSvg(svgElement, transformationsToApply);
            }
        }

        function generateLegend(dataGeoJSON) {
            dataGeoJSON.forEach(function(element, index, array) {
                var currentStyle = SettingsService.STYLES[element.type].find(function(style, index, array) {
                    return style.id = element.style.id;
                })
                LegendService.addItem(element.id,
                                      element.name,
                                      element.type,
                                      currentStyle,
                                      element.color,
                                      element.contour)
            })
        }

        function importInteractions(interactionData) {
            // Import filters.
            var filters = interactionData.querySelectorAll('filter');

            // Warning: reset to 0 to fix DER without OSM: (TODO: test if no regression)
            // we don't take the first filter, because it's the OSM Value by default in a DER
            for (var i = 0; i < filters.length; i++) {
                InteractionService.addFilter(filters[i].getAttribute('name'), filters[i].getAttribute('id'))
            }

            // Import interactions.
            var pois = interactionData.querySelectorAll('poi');
            for (var i = 0; i < pois.length; i++) {
                var actions = pois[i].querySelectorAll('action');

                for (var j = 0; j < actions.length; j++) {
                    InteractionService.setInteraction(pois[i].getAttribute('id'),
                                                        pois[i].getAttribute('label'),
                                                        actions[j].getAttribute('filter'),
                                                        actions[j].getAttribute('value'),
                                                        actions[j].getAttribute('gesture'),
                                                        actions[j].getAttribute('protocol'),
                                                        actions[j].getAttribute('uuid')
                                                    );
                }
            }

        }

    }

    angular.module(moduleApp).service('ImportService', ImportService);

    ImportService.$inject = ['LayerService', 'InteractionService', 'LegendService', 'SettingsService', 'SVGService'];

})();
