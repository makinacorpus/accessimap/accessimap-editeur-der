/**
 * @ngdoc service
 * @name accessimapEditeurDerApp.FileStoreService
 *
 * @description
 * Service storing a list of files.
 * Ensures that filenames are uniques.
 */
(function() {
    'use strict';

    function FileStoreService() {
        this.clear               = clear;
        this.addFile             = addFile;
        this.getFileByUniqueName = getFileByUniqueName;
        this.getAllFiles         = getAllFiles;
        this.getAllUniqueNames   = getAllUniqueNames;

        // Events.
        var onFileStoreChangeCallbacks = [];
        this.onFileStoreChange = onFileStoreChange;

        var files = {};

        /**
         * Clears the whole store.
         */
        function clear() {
            files = {};

            // Fire the 'onFileStoreChange' event.
            triggerCallbacks(onFileStoreChangeCallbacks);
        }

        /**
         * Adds a new file to the store.
         *
         * @param {File} file
         *   The File object to store.
         *   You may use a Blob instead if you make sure it has a "name" property, like File does.
         *
         * @returns {String}
         *   The unique filename attributed to this file freshly added to the store.
         *   If no other file has the same name, its original name is kept.
         */
        function addFile(file) {
            var unique_filename = file.name;

            var filename_increment = 2;
            while (getFileByUniqueName(unique_filename) !== null) {
                // Split filename into name and extension, so we can append an increment
                // to the name only.
                var filename_name = file.name.replace(/.[^.]+$/, '');
                var filename_extension = file.name.replace(/^.*\./, '');

                unique_filename = filename_name + '_' + filename_increment + '.' + filename_extension;

                filename_increment++;
            }

            files[unique_filename] = {
                unique_filename: unique_filename,
                file: file,
            };

            // Fire the 'onFileStoreChange' event.
            triggerCallbacks(onFileStoreChangeCallbacks);

            return unique_filename;
        }

        /**
         * Returns an Object describing and containing a file, based on its unique filename.
         *
         * @param {String} unique_filename
         *   Unique filename of the file to return.
         *
         * @returns {Object|null}
         *   Object describing the file. Structure:
         *   {
         *     'unique_filename': String,
         *     'file': File,
         *   }
         *   Returns null if no file matches given UUID.
         *   Note: If a Blob was provided initially, then that is what 'file' will contain.
         */
        function getFileByUniqueName(unique_filename) {
            if (files[unique_filename] !== undefined) {
                return files[unique_filename];
            }
            else {
                return null;
            }
        }

        /**
         * Returns the content of the whole store as an array.
         *
         * @param {Boolean} sort
         *   Optional. Set to true if output should be sorted by unique filenames (ASC).
         *   Defaults to false.
         *
         * @returns {Array}
         *   An array of objects as returned by getFileByUniqueName().
         */
        function getAllFiles(sort) {
            var allFiles = [];

            // Loop over all files to build the result array.
            var fileUniqueNames = getAllUniqueNames(sort);
            for (var i = 0; i < fileUniqueNames.length; i++) {
                var fileUniqueName = fileUniqueNames[i];
                allFiles.push(files[fileUniqueName]);
            }

            return allFiles;
        }

        /**
         * Returns the whole list of files' unique names, eventually sorted.
         *
         * @param {Boolean} sort
         *   Optional. Set to true if output should be sorted (ASC).
         *   Defaults to false.
         *
         * @returns {Array}
         *   An array of objects as returned by getFileByUniqueName().
         */
        function getAllUniqueNames(sort) {
            // Default value for 'sort'.
            if (sort === undefined) {
                sort = false;
            }

            var fileUniqueNames = Object.keys(files);

            // Sort by filename if needed.
            if (sort) {
                fileUniqueNames.sort();
            }

            return fileUniqueNames;
        }

        /**
         * Registers a callback triggered whenever the content of the FileStore changes.
         * @param {Callable} callback
         */
        function onFileStoreChange(callback) {
            onFileStoreChangeCallbacks.push(callback);
        }

        /**
         * Triggers all given callbacks.
         *
         * @param {Array} callbacks
         *   An array of callables.
         */
        function triggerCallbacks(callbacks) {
            for(var i = 0; i < callbacks.length; i++) {
                var callback = callbacks[i];
                callback();
            }
        }
    }

    angular.module(moduleApp).service('FileStoreService', FileStoreService);

    FileStoreService.$inject = [];
})();
