# Dom-to-image important notes

When writing these lines (2018-09-04 / git commit 0adab0acaadb6ff9a9479691b941ce3dc565a7b2), the library `dom-to-image` is a patched version of version `2.6.0`.

More details in the corresponding PullRequest: https://github.com/tsayen/dom-to-image/pull/246

Be carefull when upgrading this library to a new version as you might lose the fix in the above PullRequest.
