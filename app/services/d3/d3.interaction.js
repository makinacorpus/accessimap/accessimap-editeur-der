/**
 * @ngdoc service
 * @name accessimapEditeurDerApp.InteractionService
 *
 * @description
 * Service providing methods to CRUD interactions
 */
(function() {
    'use strict';

    function InteractionService(UtilService) {

        this.init                           = init;
        this.isFeatureInteractive           = isFeatureInteractive;

        // Interaction related functions.
        this.addInteraction                   = addInteraction;
        this.setInteraction                   = setInteraction;
        this.setInteractions                  = setInteractions;
        this.duplicateInteractionToAllFilters = duplicateInteractionToAllFilters;
        this.getInteractions                  = getInteractions;
        this.getInteractionByUUID             = getInteractionByUUID;
        this.getNumInteractions               = getNumInteractions;
        this.getInteractionByFeature          = getInteractionByFeature;
        this.removeInteraction                = removeInteraction;
        this.removeAllFeatureInteractions     = removeAllFeatureInteractions;
        this.removeInteractionByUUID          = removeInteractionByUUID;
        this.removeAllEmptyInteractions       = removeAllEmptyInteractions;
        this.removeAllOrphanPOIs              = removeAllOrphanPOIs;
        this.removeAllEmptyPOIs               = removeAllEmptyPOIs;

        // Filter related functions.
        this.addFilter                      = addFilter;
        this.setFilters                     = setFilters;
        this.getFilters                     = getFilters;
        this.removeFilter                   = removeFilter;

        this.getXMLExport                   = getXMLExport;

        // POI related functions
        this.updatePOIInteractionsValidityStates = updatePOIInteractionsValidityStates;
        this.updateAllInteractionsValidityStates = updateAllInteractionsValidityStates;
        this.getPoiFromId                        = getPoiFromId;

        /**
         * Hashmap of interactions.
         *
         * An interaction is a feature (point, circle, polygon, ...) that
         * will be attached to a pointer/touch event (click, double click, ...)
         * and play a specific media (sound, vibration, ...).
         */
        var interactions = {};

        /**
         * Array of filters.
         *
         * A filter define a specific type of interaction.
         * It's composed by :
         * - a name, useful for display
         * - a type of interaction : tap, double tap, ...
         * - a protocol : mp3, text-to-speech (tts), ...
         */
        var filters = [];

        /**
         * Sets interactions and filters to their initial state.
         */
        function init() {
            interactions = {};

            // Define the default filter.
            filters = [
              {
                id       : UtilService.getUUID(),
                name     : 'Défaut',
              }
            ];

        }

        /**
         * Retrieves the interactions object containing all interactions for current Drawing.
         *
         * @returns {Object}
         */
        function getInteractions() {
            return interactions;
        }

        /**
         * Helper returning the current number of interactions.
         */
        function getNumInteractions() {
          return Object.keys(interactions).length;
        }

        /**
         * Replaces all current Drawing's interactions with ones given in parameter.
         *
         * @param {Object} newInteractions
         *   New interactions object, structured as what returns getInteractions().
         */
        function setInteractions(newInteractions) {
            interactions = newInteractions;
        }

        /**
         * Duplicates given interaction to all filters, for given POI.
         * Note: Overrides any interaction of this type that would already
         * exist in target filters.
         * @param {String} poiId
         *   Poi's ID (e.g. 'poi-2').
         * @param {Object} interaction
         *   The interaction to duplicate.
         */
        function duplicateInteractionToAllFilters(poiId, interaction) {
          var poi = getPoiFromId(poiId);

          // Given POI is unknown? Do nothing.
          if (poi === null) {
            return;
          }

          // First, remove all interactions of this type (gesture) from all filters.
          poi.interactions = poi.interactions.filter(function (element) {
            return element.gesture != interaction.gesture;
          });

          // Then copy given interaction to all filters.
          var filters = getFilters();
          for (var i = 0; i < filters.length; i++) {
            var filterId = filters[i].id;

            setInteraction(
              poiId,
              null,
              filterId,
              interaction.value,
              interaction.gesture,
              interaction.protocol
            );
          }
        }

       /**
         * Returns all filters.
         *
         * @returns {Array}
         *   List of filter objects.
         */
        function getFilters() {
            return filters;
        }

        /**
         * Replaces all current Drawing's filters with ones given in parameter.
         *
         * @param {Object} newFilters
         *   New filters object, strctured as what returns getFilters().
         */
        function setFilters(newFilters) {
            filters = newFilters;
        }

        /**
         * @ngdoc method
         * @name  isFeatureInteractive
         * @methodOf accessimapEditeurDerApp.InteractionService
         *
         * @description
         * Return if the feature is an interactive one
         *
         * @param  {Object}  feature
         * Feature to be checked
         *
         * @return {Boolean}
         * True if interactive, false if not
         */
        function isFeatureInteractive(feature) {
            var featureIid = feature.attr('data-link');

            if (featureIid !== undefined) {
                var matchingInteraction = interactions['poi-' + featureIid];

                return (matchingInteraction !== undefined);
            }

            return false;
        }

        /**
         * Deletes an interaction based on given POI's id and interaction's index.
         *
         * @deprecated You should use removeInteractionByUUID() instead.
         *
         * @param {String} poiId
         *   POI's identifier.
         * @param {int} interactionIndex
         *   Index of teh interaction to delete from the POI's `interactions` property.
         *   E.g.: If you provide 2, the third interaction would get deleted.
         */
        function removeInteraction(poiId, interactionIndex) {
            interactions[poiId].interactions.splice(interactionIndex, 1);
            updateAllInteractionsValidityStates();
        }

        /**
         * Removes all the interactions linked to a Feature, based on given poiId.
         * @param {String} poiId
         *   POI's identifier.
         */
        function removeAllFeatureInteractions(poiId) {
            delete interactions[poiId];
            updateAllInteractionsValidityStates();
        }

        /**
         * Removes interaction identified by given UUID.
         * @param {String} uuid
         *   Interaction's UUID.
         */
        function removeInteractionByUUID(uuid) {
            // Loop over POIs.
            var poiIds = Object.keys(interactions);
            poiIds.findIndex(function(poiId) {
                // Look for the interaction to remove.
                var interactionIndex = interactions[poiId].interactions.findIndex(function(interaction){
                    if (interaction.uuid === uuid) {
                        return true;
                    }
                });

                // If matching interaction was found, remove it from the list.
                if (interactionIndex !== -1) {
                    interactions[poiId].interactions.splice(interactionIndex, 1);

                    // Also check related POI's state.
                    updatePOIInteractionsValidityStates(interactions[poiId]);

                    // Stop looping over POIs as we found the interaction to be removed.
                    return true;
                }
            });
        }

        /**
         * Gets an interaction identified by given UUID.
         * @param {String} uuid
         *   Interaction's UUID.
         * @returns {Object|null}
         *   The sought interaction, or null if none matched.
         */
        function getInteractionByUUID(uuid) {
            // Loop over POIs.
            var poiIds = Object.keys(interactions);
            for (var i = 0; i < poiIds.length; i++) {
                var poiIndex = poiIds[i];
                var poiInteractions = interactions[poiIndex].interactions

                // Look for an interaction with given UUID.
                var interaction = poiInteractions.find(function(element) {
                    return (element.uuid == uuid);
                });

                // Sought interaction found: return it immediately.
                if (typeof interaction !== 'undefined') {
                    return interaction;
                }
            }

            // No match found.
            return null;
        }

        /**
         * Deletes all interactions that have no value.
         * @param {String} exceptionPoiId
         *   If a POI should be excluded from this cleaning process.
         *   Should be POI's string ID (e.g.: 'poi-3')
         */
        function removeAllEmptyInteractions(exceptionPoiId) {
          if (typeof exceptionPoiId === 'undefined') {
            exceptionPoiId = '';
          }

          // Loop over POIs and look for empty interactions.
          var poiIds = Object.keys(interactions);
          for (var i = 0; i < poiIds.length; i++) {
            var poiId = poiIds[i];

            // Skip a POI if required.
            if (poiId === exceptionPoiId) {
              continue;
            }

            // Loop over interactions to spot empty ones.
            var emptyInteractionUUIDs = [];
            for (var j = 0; j < interactions[poiId].interactions.length; j++) {
              var interaction = interactions[poiId].interactions[j];

              if (typeof interaction.value === 'undefined' || interaction.value === null || interaction.value === '') {
                emptyInteractionUUIDs.push(interaction.uuid);
              }
            }

            // Delete all empty interactions.
            for (var j = 0; j < emptyInteractionUUIDs.length; j++) {
              var uuid = emptyInteractionUUIDs[j]
              removeInteractionByUUID(uuid);
            }
          }
        }

        /**
         * Deletes all POIs that have no interactions, and no label.
         * @param {String} exceptionPoiId
         *   If a POI should be excluded from this cleaning process.
         *   Should be POI's string ID (e.g.: 'poi-3')
         */
        function removeAllEmptyPOIs(exceptionPoiId) {
          if (typeof exceptionPoiId === 'undefined') {
            exceptionPoiId = '';
          }

          // Loop over POIs and look for the ones that have no interactions.
          var poiIds = Object.keys(interactions);
          for (var i = 0; i < poiIds.length; i++) {
            var poiId = poiIds[i];

            // Skip a POI if required.
            if (poiId === exceptionPoiId) {
              continue;
            }

            // Test whether this POI has interactions.
            var hasInteractions = true;
            if (typeof interactions[poiId].interactions === 'undefined' || interactions[poiId].interactions.length === 0) {
              hasInteractions = false;
            }

            // No interactions? If it has no label either, delete it.
            if (!hasInteractions && (typeof interactions[poiId].label === 'undefined' || interactions[poiId].label === null || interactions[poiId].label === '')) {
              delete interactions[poiId];
            }
          }
        }

        /**
         * Removes all POIs that are not linked to any Feature.
         */
        function removeAllOrphanPOIs() {
            var orphanPoiIds = [];

            // Loop over POIs and look for their matching features.
            var poiIds = Object.keys(interactions);
            for (var i = 0; i < poiIds.length; i++) {
                var poiId = poiIds[i];
                var feature = _getFeatureFromPoiId(poiId);

                // No matching feature? Flag this POI as orphan.
                if (feature === null) {
                    orphanPoiIds.push(poiId);
                }
            }

            // Delete all orphan POIs.
            for (var i = 0; i < orphanPoiIds.length; i++) {
                var poiId = orphanPoiIds[i];
                removeAllFeatureInteractions(poiId);
            }
        }

        /**
         * Returns a Feature based on given POI's ID.
         *
         * @param {String} poiId
         *   Poi's ID (e.g. 'poi-1').
         * @returns {Object}
         *   The corresponding feature object.
         *   Returns null if none was found.
         */
        function _getFeatureFromPoiId(poiId) {
            var poiIdParts = poiId.split('-');
            var featureId = null;

            if (poiIdParts.length >= 2) {
                featureId = poiIdParts[1]
            }
            else {
                return null;
            }

            var feature = d3.select('[data-link="' + featureId + '"]')[0];
            if (typeof feature[0] != 'undefined') {
                feature = feature[0];
            }
            else {
                feature = null;
            }

            return feature;
        }

        /**
         * Checks the state validity of all the interactions of given POI.
         * Sets the POI's property "invalidFilterIds" to an array listing ids
         * of all the filters that are invalid. If none, array will be empty.
         *
         * @param {Object} poi
         *   The interactions object for a POI. As held by the `interactions` variable.
         */
        function updatePOIInteractionsValidityStates(poi) {
            // Will hold the list of filter ids misconfigured.
            var invalidFilterIds = [];

            // Object holding the list of filters, and for each of them,
            // the list of gestures configured. Will make the spotting of
            // duplicate gestures within a filter easier.
            // Structure : {'filter-id1': {'gesture1': true, 'gesture2': true}, 'filter-id2': ...}
            var filtersGestures = {};

            // Spot duplicate couples "filter"-"gesture".
            for (var i = 0; i < poi.interactions.length; i++) {
                var interaction = poi.interactions[i];
                var filter = interaction.filter;
                var gesture = interaction.gesture;

                if (filtersGestures[filter] === undefined) {
                    filtersGestures[filter] = {};
                }

                if (filtersGestures[filter][gesture] === undefined) {
                    // This gesture was not already defined for this filter yet.
                    // Flag it as existing so we can spot any potential duplicates
                    // later.
                    filtersGestures[filter][gesture] = true;
                }
                else {
                    // This gesture is already defined for this filter.
                    // Tag this filter as invalid.
                    invalidFilterIds.push(filter);
                }
            }

            // If any invalid filters were found, they will be listed in
            // the "invalidFilterIds" property of the POI.
            poi.invalidFilterIds = invalidFilterIds;
        }

        /**
         * Checks the validity states of all interactions.
         */
        function updateAllInteractionsValidityStates() {
            // Loop over POIs and check their interactions.
            var poiIds = Object.keys(interactions);
            for (var i = 0; i < poiIds.length; i++) {
                var poiId = poiIds[i];

                updatePOIInteractionsValidityStates(interactions[poiId]);
            }
        }

        /**
         * Returns a POI object from given Id.
         * @param {String} poiId
         *   Poi's ID (e.g. 'poi-2').
         * @return {Object|null}
         *   POI object. Returns null if it couldn't be found.
         */
        function getPoiFromId(poiId) {
          if (typeof interactions[poiId] !== 'undefined') {
            return interactions[poiId];
          }

          return null;
        }

        /**
         * @ngdoc method
         * @name  addInteraction
         * @methodOf accessimapEditeurDerApp.InteractionService
         *
         * @description
         * Add an interaction on a feature
         *
         * @param {string} poiId
         *   ID of added poi
         * @param {String} label
         *   Label of the POI.
         *   Setting it to null or undefined would make it
         *   default to empty string.
         * @param {string} filterId
         *   ID of added filter
         * @param {string} value
         *   Initial value to attribute to the interaction.
         *   Optional. Defaults to empty string.
         */
        function addInteraction(poiId, label, filterId, value) {
            setInteraction(poiId, label, filterId, value);
        }

        /**
         * Returns the interaction linked to given feature.
         *
         * @param {Object} feature
         * @returns {Object}
         *   Interaction linked to given feature.
         *   Null if none exists yet.
         */
        function getInteractionByFeature(feature) {
            var featureIid = feature.attr('data-link');

            // No Iid? Abort.
            if(!featureIid) {
                return null;
            }

            // No interaction for given feature? Abort.
            if (interactions['poi-' + featureIid] === undefined) {
                return null;
            }

            // Return found interaction.
            return interactions['poi-' + featureIid];

        }

        /**
         * Sets an interaction {filter: value} for a feature (id).
         *
         * @param {String} poiId
         *   Id of the POI.
         * @param {String} label
         *   Label of the POI. Set to NULL to leave current value as is.
         *   If no value currently exists, defaults to empty string.
         * @param {String} filterId
         *   Filter unique identifier to link the interaction to.
         *   Defaults to the first one.
         * @param {String} value
         *   Interaction value.
         * @param {String} gesture
         *   Interaction gesture (e.g.: 'tap').
         *   Defaults to 'tap'.
         * @param {String} protocol
         *   Interaction protocol (e.g.: 'tts').
         *   Defaults to 'tts'.
         * @param {String} interactionUUID
         *   Interaction UUID. If an interaction with this UUID already exists,
         *   it will be replaced. If not provided or null, a new UUID will be attributed
         *   to it.
         */

        function setInteraction(poiId, label, filterId, value, gesture, protocol, interactionUUID) {
            // Defaults.
            if (typeof label === 'undefined') {
                label = null;
            }

            if (!interactions[poiId]) {
                interactions[poiId] = {
                    'id': poiId,
                    'label': '',
                    'interactions': []
                };
            }

            // Default label.
            if (label !== null) {
              interactions[poiId].label = label;
            }

            var existingInteractionIndex = -1;
            if (interactionUUID === undefined || interactionUUID === null) {
                interactionUUID = UtilService.getUUID();
            }
            else {
                // Look for existing UUID.
                existingInteractionIndex = interactions[poiId].interactions.findIndex(function(interaction) {
                    if (interaction.uuid === interactionUUID) {
                        return true;
                    }
                });
            }

            // No filter given? Look for first one.
            if (filterId === null || typeof filterId === 'undefined') {
              filterId = null;
              var firstFilter = getFirstFilter();

              if (firstFilter !== null) {
                  filterId = firstFilter.id;
              }
            }

            if (filterId !== null) {
              var interactionData = {
                  uuid: interactionUUID,
                  filter: filterId,
                  value: value || '',
                  gesture  : gesture || 'tap',
                  protocol : protocol || 'tts'
              }

              // If an interaction with this UUID already exists, replace it.
              if (existingInteractionIndex !== -1) {
                  interactions[poiId].interactions[existingInteractionIndex] = interactionData;
              }
              // No interaction exists with this UUID, add a new one.
              else {
                  interactions[poiId].interactions.push(interactionData);
              }
            }

            // Update POI's validity state.
            updatePOIInteractionsValidityStates(interactions[poiId]);
        }

        /**
         * Helper returning the first filter that is not the default one.
         *
         * @return {Object|null}
         *   A filter object, or null if there are none.
         */
        function getFirstFilter() {
          if (filters.length == 0) {
            return null;
          }

          return filters[0];
        }

        /**
         * Adds a filter.
         *
         * @param {String} name
         *   Optional. Filter's label.
         *   Defaults to empty string.
         * @param {String} id
         *   Optional. Filter's unique identifier.
         *   Defaults to a UUID.
         */
        function addFilter(name, id) {
            var filterIndex = null;
            var newFilter = {
                id       : id ? id : UtilService.getUUID(),
                name     : name ? name : '',
            };

            filters.map(function(element, index) {
                if (element.id === id) {
                    filterIndex = index
                }
            })

            // Check if filter id is already present
            if (filterIndex !== null) {
                filters[filterIndex] = newFilter;
            } else {
                filters.push(newFilter);
            }
        }

        /**
         * Removes a filter.
         *
         * @param {String} id
         *   Unique identifier of the filter to remove.
         */
        function removeFilter(id) {
            filters = filters.filter(function removeFilter(element, index) {
                return element.id !== id;
            });
        }

        function getXMLExport() {
            var translateX;
            var translateY;
            var xmlToExport = '<?xml version="1.0" encoding="UTF-8"?>\n';

            if (filters.length > 0) {

                var nodeXML = d3.select(document.createElement('exportXML')),
                    config = nodeXML.append('config');

                config.append('filters')
                    .selectAll('filter')
                    .data(filters)
                    .enter()
                    .append('filter')
                    .attr('id', function(d) {
                        return d.id;
                    })
                    .attr('name', function(d) {
                        return d.name;
                    })
                    .attr('gesture', function(d) {
                        return d.gesture;
                    })
                    .attr('protocol', function(d) {
                        return d.protocol;
                    })
                    .attr('expandable', function(d) {
                        // return d.expandable;
                        return false; // TODO: qu'est ce qu'expandable ?
                    });

                config.append('pois')
                    .selectAll('poi')
                    .data(Object.keys(interactions))
                    .enter()
                    .append('poi')
                    .attr('id', function(d) {
                      return interactions[d].id;
                    })
                    .attr('label', function(d) {
                      return interactions[d].label;
                    })
                    .each(function(d) {
                        var poiInteractions = interactions[d].interactions;

                        translateX = 0;
                        translateY = 0;

                        // get the bounding box of the current POI,
                        // no matter if it is a drawing or a geojson feature
                        // > no selection of a specific svg, we search in the entire DOM
                        var bbox, poi;
                        d3.selectAll('path')[0]
                            .forEach(function(shape) {
                                if ('poi-' + d3.select(shape).attr('data-link') === d) {
                                  if (d3.select(shape).attr("transform")) {
                                    translateX=parseFloat(d3.select(shape).attr("transform").replace('translate(', '').replace(')', '').split(",")[0]);
                                    translateY=parseFloat(d3.select(shape).attr("transform").replace('translate(', '').replace(')', '').split(",")[1]);
                                  }

                                  bbox = d3.select(shape).node().getBBox();
                                }
                            });

                        poi = d3.select(this).attr('id', d);
                        if (bbox) {
                            poi.attr('x', bbox.x + translateX);
                            poi.attr('y', bbox.y + translateY);
                            poi.attr('width', bbox.width);
                            poi.attr('height', bbox.height);
                        }

                        // exporting actions for the current POI
                        var actions = poi.append('actions');
                        d3.keys(poiInteractions).forEach(function(key) {
                            actions.append('action')
                                .attr('uuid', poiInteractions[key].uuid)
                                .attr('gesture', poiInteractions[key].gesture)
                                .attr('filter', poiInteractions[key].filter)
                                .attr('value', poiInteractions[key].value)
                                .attr('protocol', poiInteractions[key].protocol);
                        });
                    });

                xmlToExport += (new XMLSerializer()).serializeToString(config.node());

            }

            return xmlToExport;
        }
    }

    angular.module(moduleApp).service('InteractionService', InteractionService);

    InteractionService.$inject = ['UtilService'];

})();
