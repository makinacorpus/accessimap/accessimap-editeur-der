<!---
Merci de lire ceci !

Avant d'ouvrir une nouvelle issue, merci de vérifier au préalable 
avec le moteur de recherche qu'il n'y a pas d'issues existantes
avec le label "Bug".

--->

### Résumé

(Résumer l'anomalie rencontrée)

### Scénario de reproduction

(Comment reproduire l'anomalie - ceci est très important !)

* *Système d'exploitation*: (Windows / Linux / MacOS)
* *Version utilisée*: (Web+Chrome / Web+Firefox / Web+???)

### Quelle est l'anomalie rencontrée ?

(Ce qui se passe aujourd'hui et qui est anormal)

### Quel serait le comportement *attendu* ?

(Ce qui devrait se passer normalement)

### Capture d'écran / autres infos d'anomalie

(Copier ici des captures d'écrans si possible, ou mettre les liens des fichiers en cause
ou toute autre info utile)

### Corrections éventuelles

(Si vous avez d'autres idées sur l'anomalie, n'hésitez pas à nous en dire plus par ici)

/label ~Bug
