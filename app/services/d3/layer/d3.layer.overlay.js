/**
 * @ngdoc service
 * @name accessimapEditeurDerApp.LayerOverlayService
 *
 * @description
 * Service providing the management of overlay layer
 */
(function() {
    'use strict';

    function LayerOverlayService(SettingsService) {

        this.createLayer     = createLayer;
        this.refresh         = refresh;
        this.draw            = draw;
        this.getCenter       = getCenter;
        this.getTotalTranslation = getTotalTranslation;
        this.getRelativeTranslation = getRelativeTranslation;
        this.getSize         = getSize;
        this.setFormat       = setFormat;

        this.getFormat       = function() { return SettingsService.FORMATS[_format] }

        var
            _format,
            _width,
            _height,
            _target,
            _projection,
            _lastTranslationX,
            _lastTranslationY,
            _outterBackgroundPath,

            _marginGroup,
            _margin;

            // Menu placeholders.
        var _filterMenuGroup,
            _filterMenuArea,
            _filterMenuIcon,
            _mainMenuGroup,
            _mainMenuArea,
            _mainMenuIcon;

        /**
         * Creates the overlay layer, including outter background and
         * menu placeholders.
         *
         * @param  {Object} target
         * d3 area
         *
         * @param  {integer} format
         * Format identifier (see SettingsService.FORMATS).
         *
         * @param  {function} projection
         * Projection function to use for conversion between GPS & layer point
         */
        function createLayer(target, format, projection) {
            _margin         = SettingsService.margin;
            _target         = target;
            _projection     = projection;

            // Menu placeholders.
            _filterMenuGroup = _target.append('g').attr('id', 'filter-menu-placeholder');
            _filterMenuArea  = _filterMenuGroup.append('rect');
            _filterMenuIcon  = _filterMenuGroup.append('path');
            _mainMenuGroup   = _target.append('g').attr('id', 'main-menu-placeholder');
            _mainMenuArea    = _mainMenuGroup.append('rect');
            _mainMenuIcon    = _mainMenuGroup.append('path');

            _marginGroup          = _target.append('g').attr('id', 'outter-margin-layer');
            _outterBackgroundPath = _marginGroup.append('path');

            setFormat(format);
        }

        /**
         * Creates the margin path.
         */
        function createOutterBackgroundPath() {
            var w40 = _width - _margin,
                h40 = _height - _margin;

            _outterBackgroundPath.attr('d', function() {
                    var outer = 'M -10000 -10000 L -10000 '
                                    + ( _height + 10000 )
                                    + ' L '
                                    + ( _width + 10000 )
                                    + ' '
                                    + ( _height + 10000 )
                                    + ' L '
                                    + ( _width + 10000 )
                                    + ' -10000 L -10000 -10000 z ',
                        inner = 'M 0 0 L '
                                    + _width
                                    + ' 0 L '
                                    + _width
                                    + ' '
                                    + _height
                                    + ' L ' + 0 + ' '
                                    + _height + ' z';

                    return outer + inner;
                })
                .style('pointer-events', 'none')
                .style('fill', '#bbb')
                .style('fill-opacity', '.5')
                .classed('notDeletable', true);
        };

        function getCenter() {
            return _projection.layerPointToLatLng([( _width / 2 ) + _lastTranslationX,
                                                    ( _height / 2 ) + _lastTranslationY]);
        }

        /**
         * Creates the menu placeholders.
         */
        function createMenuPlaceholders() {
            applyMenuPlaceholderStyle(_filterMenuArea, _filterMenuIcon, 'filter');
            applyMenuPlaceholderStyle(_mainMenuArea, _mainMenuIcon, 'main');
        }

        /**
         * Helper applying visual styles to menu placholders.
         * Behaves differently depending on given 'menuType'.
         *
         * @param {Object} areaElement
         *   SVG element to style (background of the menu placeholder).
         * @param {Object} iconElement
         *   SVG element to style (icon of the menu placeholder).
         * @param {String} menuType
         *   Identifies the type of menu placeholder.
         *   Applied styles are a bit different based on this parameter.
         *   You should set it to either 'filter' (for the filter menu placholder)
         *   or 'main' (for the main menu placeholder).
         *   Any other value may have unexpected behaviors.
         */
        function applyMenuPlaceholderStyle(areaElement, iconElement, menuType) {
            var placeholderWidth = 80;
            var placeholderHeight = 40;

            // Apply common properties to the area element.
            areaElement.attr('y', _margin)
                .attr('width', placeholderWidth)
                .attr('height', placeholderHeight)
                .style('fill', '#ffffff')
                .style('pointer-events', 'none')
                .attr('id', 'filterMenuPlaceholder')
                .classed('notDeletable', true);

            // Apply type dependent properties to the area element.
            switch(menuType) {
                case 'filter':
                    areaElement.attr('x', _margin);
                    break;

                case 'main':
                    areaElement.attr('x', _width - _margin - placeholderWidth);
                    break;
            }

            // Apply common properties to the icon element.
            iconElement.classed('notDeletable', true);

            var areaCenterX = parseFloat(areaElement.attr('x')) + (placeholderWidth/2);
            var areaCenterY = parseFloat(areaElement.attr('y')) + (placeholderHeight/2);

            // Apply type dependent properties to the icon element.
            switch(menuType) {
                case 'filter':
                    iconElement.attr(
                        'd',
                        // Starts from the center of the area.
                        'M ' + areaCenterX + ',' + areaCenterY + ' ' +
                        // Adjust start position so it fits the area.
                        'm -20,5 ' +
                        'a 5,5 0 0 0 5,-4 ' +
                        // Horizontal line.
                        'l 40,0 0,-2 -40,0 ' +
                        'a 5,5 0 0 0 -5,-4 5,5 0 0 0 -5,5 5,5 0 0 0 5,5 z');
                    break;

                case 'main':
                    iconElement.attr(
                        'd',
                        // Starts from the center of the area.
                        'M ' + areaCenterX + ',' + areaCenterY + ' ' +
                        // Adjust start position so it fits the area.
                        'm 20,-5 ' +
                        'a 5,5 0 0 0 -5,4 ' +
                        // Horizontal line.
                        'l -40,0 0,2 40,0 ' +
                        'a 5,5 0 0 0 5,4 5,5 0 0 0 5,-5 5,5 0 0 0 -5,-5 z');
                    break;
            }
        }

        /**
         * @ngdoc method
         * @name  draw
         * @methodOf accessimapEditeurDerApp.LayerOverlayService
         *
         * @description
         * Draw margin & menu placeholders.
         *
         * @param  {integer} width
         * Overlay's width
         *
         * @param  {integer} height
         * Overlay's height
         *
         */
        function draw() {
            createOutterBackgroundPath();
            createMenuPlaceholders();
        }

        function setFormat(format) {
            _format     = format;
            _width      = SettingsService.FORMATS[format].width / SettingsService.ratioPixelPoint;
            _height     = SettingsService.FORMATS[format].height / SettingsService.ratioPixelPoint;

            draw();
        }

        /**
         * @ngdoc method
         * @name  refresh
         * @methodOf accessimapEditeurDerApp.LayerOverlayService
         *
         * @description
         * Function moving the overlay svg, thanks to map movements...
         *
         * This function has to be used only if we want the overlay be 'fixed'
         *
         * TODO: clear the dependencies to map... maybe, give the responsability to the map
         * and so, thanks to a 'class', we could 'freeze' the overlay thanks to this calc
         */
        function refresh(size, pixelOrigin, pixelBoundMin) {
            // x,y are coordinates to position overlay
            // coordinates 0,0 are not the top left, but the point at the middle left
            _lastTranslationX =
                // to get x, we calc the space between left and the overlay
                ( ( size.x - _width) / 2 )
                // and we substract the difference between the original point of the map
                // and the actual bounding topleft point
                - (pixelOrigin.x - pixelBoundMin.x),

            _lastTranslationY =
                // to get y, we calc the space between the middle axe and the top of the overlay
                _height / -2
                // and we substract the difference between the original point of the map
                // and the actual bounding topleft point
                - (pixelOrigin.y - pixelBoundMin.y - size.y / 2);

            _marginGroup.attr("transform", "translate(" + (_lastTranslationX ) +"," + (_lastTranslationY) + ")");
            _filterMenuGroup.attr("transform", "translate(" + (_lastTranslationX ) +"," + (_lastTranslationY) + ")");
            _mainMenuGroup.attr("transform", "translate(" + (_lastTranslationX ) +"," + (_lastTranslationY) + ")");
        }

        /**
         * Returns the total translation of the "overlayer" + "outter-margin-layer" layers.
         * @return {Object}
         *   {x: int, y: int} representing the translation on x axis & y axis.
         */
        function getTotalTranslation() {
            var translateScaleOverlayGroup = _target.attr('transform'),

                translateOverlayGroup = ( translateScaleOverlayGroup === null ) ? null
                    : translateScaleOverlayGroup.substring(translateScaleOverlayGroup.indexOf('(') + 1,
                                                            translateScaleOverlayGroup.indexOf(')')),

                translateOverlayGroupArray = ( translateOverlayGroup === null ) ? [0, 0]
                    : translateOverlayGroup.slice(0, translateOverlayGroup.length).split(','),

                translateOverlayArray = [ _lastTranslationX, _lastTranslationY ]

            return { x: ( parseInt(translateOverlayArray[0]) + parseInt(translateOverlayGroupArray[0]) ),
                     y: ( parseInt(translateOverlayArray[1]) + parseInt(translateOverlayGroupArray[1]) ) }
        }

        /**
         * Returns the translation of inner layer only ("outter-margin-layer").
         * @return {Object}
         *   {x: int, y: int} representing the translation on x axis & y axis.
         */
        function getRelativeTranslation() {
          return {
            x: ( parseInt(_lastTranslationX) ),
            y: ( parseInt(_lastTranslationY) ),
          }
        }

        /**
         * @ngdoc method
         * @name  getSize
         * @methodOf accessimapEditeurDerApp.LayerOverlayService
         *
         * @description
         * return the size of the layer, representing the size of the drawing
         *
         * @return {Object}
         * {width, height}
         *
         */
        function getSize() {
            return {width: _width, height: _height}
        }

    }

    angular.module(moduleApp).service('LayerOverlayService', LayerOverlayService);

    LayerOverlayService.$inject = ['SettingsService'];

})();
