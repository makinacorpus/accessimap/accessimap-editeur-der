/**
 * @ngdoc service
 * @name accessimapEditeurDerApp.SelectPathService
 *
 * @description
 * Provide function to calculate the 'select path' of a feature
 *
 */
(function() {
    'use strict';

    function SelectPathService(EditPropertiesService) {

        this.calcSelectPath = calcSelectPath;
        this.addTo          = addTo;
        this.removeTo       = removeTo;

        this.highlightFeature            = highlightFeature;
        this.unlightFeature              = unlightFeature;
        this.visuallySelectFeature       = visuallySelectFeature;
        this.visuallyUnselectAllFeatures = visuallyUnselectAllFeatures;

        /**
         * @ngdoc method
         * @name  calcSelectPath
         * @methodOf accessimapEditeurDerApp.SelectPathService
         *
         * @description
         * Calculate the select path around an element.
         * It's a rect identical of the bbox.
         *
         * @param  {Object} feature
         * d3 object
         *
         * @return {DOMElement}
         * element to add to DOM representing the empty comfort
         */
        function calcSelectPath(feature) {

            var el = feature.node(),
                bbox = el.getBBox(),
                type = feature.attr('data-type'),
                selectPath,
                path = feature.attr('d'),
                transformString = null || feature.attr('transform');

            selectPath = document.createElementNS('http://www.w3.org/2000/svg', 'rect');

            d3.select(selectPath)
                .attr('x', bbox.x - 4)
                .attr('y', bbox.y - 4)
                .attr('width', bbox.width + 8)
                .attr('height', bbox.height + 8)
                .attr('data-type', 'select-path')
                .attr('fill', 'none')
                .attr('stroke', 'red')
                .attr('stroke-width', '2')
                .attr('transform', transformString);

            return selectPath;

        }

        function calcClickPath(feature) {
            var el = feature.node(),
                bbox = el.getBBox(),
                type = feature.attr('data-type'),
                selectPath,
                path = feature.attr('d'),
                transformString = null || feature.attr('transform');

            selectPath = document.createElementNS('http://www.w3.org/2000/svg', 'rect');

            d3.select(selectPath)
                .attr('x', bbox.x - 2)
                .attr('y', bbox.y - 2)
                .attr('width', bbox.width + 4)
                .attr('height', bbox.height + 4)
                .attr('data-type', 'clicked-path')
                .attr('fill', 'none')
                .attr('stroke', '#333')
                .attr('stroke-width', '1')
                .style('stroke-dasharray', ('2,4')) // make the stroke dashed
                .attr('transform', transformString);

            return selectPath;
        }

        function addTo(nodes, callbackProperties) {
            nodes.style('cursor', 'crosshair')
                .on('mouseover', function(event) {
                    var feature = d3.select(this);
                    highlightFeature(feature);
                })
                .on('mouseout', function(event) {
                    var feature = d3.select(this);
                    unlightFeature(feature);
                })
                .on('click', function(event) {
                    var feature = d3.select(this);
                    visuallyUnselectAllFeatures();
                    visuallySelectFeature(feature);
                    callbackProperties && callbackProperties(feature);
                })
        }

        function removeTo(nodes) {
            nodes.style('cursor', '')
                 .on('mouseover', function() {})
                 .on('mouseout', function() {})
                 .on('click', function() {})
        }

        /**
         * Visually emphasises given feature on screen by adding a surrounding shape to it.
         *
         * @param {Object} feature
         *   The feature to highlight.
         */
        function highlightFeature(feature) {
            // Compute the shape that will visually highlights the feature.
            var selectPath = calcSelectPath(feature);

            // Add it to the dom.L
            feature.node().parentNode.appendChild(selectPath);
        }

        /**
         * Removes the emphasise that was added to given feature.
         *
         * @param {Object} feature
         *   The feature to "unlight".
         */
        function unlightFeature(feature) {
            // Remove the highlight shape from the dom.
            d3.select(feature.node().parentNode)
                .selectAll('[data-type="select-path"]')
                .remove();
        }

        /**
         * Visually selects given feature on screen by adding a surrounding shape to it.
         *
         * @param {Object} feature
         *   The feature to select.
         */
        function visuallySelectFeature(feature) {
            var selectPath = calcClickPath(feature);
            feature.node().parentNode.appendChild(selectPath);
        }

        /**
         * Unselects all features (removes all corresponding shapes from the dom).
         */
        function visuallyUnselectAllFeatures() {
            d3.selectAll('[data-type="clicked-path"]').remove();
        }
    }

    angular.module(moduleApp).service('SelectPathService', SelectPathService);

    SelectPathService.$inject = ['EditPropertiesService'];

})();
